package com.n1ark.whereitends.world;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.n1ark.whereitends.entity.Entity;
import com.n1ark.whereitends.particle.Particle;
import com.n1ark.whereitends.util.PVector;

public class World {
	/**
	 * The name of the file where all the world's data will be stored.
	 */
	public static final String WORLD_DATA_FILE = "data.world";
	
	/**
	 * The map containing all chunks in this world, depending on their index.
	 */
	private ConcurrentHashMap<Integer, Chunk> chunks;
	
	/**
	 * A List with all the entities currently living in the world.
	 */
	private List<Entity> entities;
	
	/**
	 * A List with all the particles currently existing in the world.
	 */
	private List<Particle> particles;
	
	/**
	 * The seed used to generate the world.
	 */
	private long seed;
	
	/**
	 * The name of this world, will be used when in the world selection menu, and to save the file.
	 */
	private String name;
	
	/**
	 * The color for the background of the world.
	 */
	private Color color;
	
	/**
	 * The GenerationMode, deciding on how the chunks will be generated.
	 */
	private GenerationMode generationMode;
	
	/**
	 * The starting Location, where the Player should spawn.
	 */
	private PVector start;
	
	/**
	 * Creates a world.
	//TODO Maybe trying to add generation, and options in the constructor to be able to create custom worlds easily.
	 * @param name The name of the world.
	 * @param genMode The way the chunks will be generated.
	 * @param seed The seed by which the world and all the chuks will be generated.
	 */
	public World(String name, GenerationMode genMode, long seed) {
		this.seed = seed;
		this.name = name;
		this.generationMode = genMode;
		this.entities = new CopyOnWriteArrayList<>();
		this.particles = new CopyOnWriteArrayList<>();
		this.color = new Color(0xFFBBCCFF);
		this.chunks = new ConcurrentHashMap<>();
		this.start = new PVector(0, 70);
	}
	
	/**
	 * @return The seed used to generate the world.
	 */
	public long getSeed() {
		return this.seed;
	}
	
	/**
	 * @return The name of this world, that will appear in the world selection, or when saving the world.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return The GenerationMode, that will decide on how the chunks are generated.
	 */
	public GenerationMode getGenerationMode() {
		return this.generationMode;
	}
	
	/**
	 * @return The color to be used for the world's background.
	 */
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Will set this world's background color.
	 * @param c The new background color.
	 */
	public void setColor(Color c) {
		this.color = c;
	}
	
	/**
	 * @return The list containing all currently living entities in the world.
	 */
	public List<Entity> getEntities(){
		return this.entities;
	}
	
	/**
	 * @return The list containing all the currently existing particles in the world.
	 */
	public List<Particle> getParticles(){
		return this.particles;
	}
	
	/**
	 * Will add a Particle to the existing ones.
	 * @param p The new particle.
	 */
	public void addParticle(Particle p) {
		this.particles.add(p);
	}
	
	/**
	 * Will remove the given Particle from the existing particles.
	 * @param p The remove Particle.
	 * @return If the given Particle was in the List.
	 */
	public boolean removeParticle(Particle p) {
		return this.particles.remove(p);
	}
	
	/**
	 * @return The location where the Player should spawn.
	 */
	public PVector getSpawnLocation() {
		return this.start.get();
	}
	
	/**
	 * Will set the new location, for where the Player should spawn.
	 * @param location The new Location. The World will copy the coordinates.
	 */
	public void setSpawnLocation(PVector location) {
		this.start.set(location);
	}
	
	/**
	 * @param x The X coordinate of the block.
	 * @param y The Y coordinate of the block.
	 * @return The block at the specified location in the world. If no block is found there, a new
	 * chunk is generated with the seed.
	 */
	public Block get(int x, int y) {
		if(y < 0)
			return Block.AIR;
		int ind = x >= 0 ? x / Chunk.X_SIZE : x / Chunk.X_SIZE - 1;
		int bx  = x >= 0 ? x % Chunk.X_SIZE : Chunk.X_SIZE + x % Chunk.X_SIZE - 1;
		try {
			return this.chunks.get(ind).getBlock(bx, y);
		} catch (NullPointerException e) {
			Chunk c = new Chunk(ind);
			c.generate(this.generationMode, this.seed);
			this.chunks.put(ind, c);
			System.out.println("Generated " + ind);
			return this.chunks.get(ind).getBlock(bx, y);
		} catch (ArrayIndexOutOfBoundsException e) {
			return Block.AIR;
		}
	}
	
	public void set(Block b, int x, int y) {
		int ind = x >= 0 ? x / Chunk.X_SIZE : x / Chunk.X_SIZE - 1;
		int bx  = x >= 0 ? x % Chunk.X_SIZE : Chunk.X_SIZE + x % Chunk.X_SIZE - 1;
		try {
			this.chunks.get(ind).setBlock(b, bx, y);
		} catch (NullPointerException e) {
			Chunk c = new Chunk(ind);
			c.generate(this.generationMode, this.seed);
			this.chunks.put(ind, c);
			System.out.println("Generated " + ind);
			this.chunks.get(ind).setBlock(b, bx, y);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save the world to the specified root. This will generate multiple <code>.chunk</code> files
	 * inside of this folder.
	 * @param f The folder in which the file will be saved.
	 * @throws IOException
	 */
	public void saveTo(Path f) throws IOException {
		long a = System.currentTimeMillis();
		Path directory = f.resolve(this.name);
		if (!directory.toFile().exists())
			directory.toFile().mkdir();
		Path file = directory.resolve(WORLD_DATA_FILE);
		Files.write(
				file,
				Arrays.asList(
						this.name,
						Long.toString(this.seed),
						Integer.toString(this.color.getRGB()),
						Float.toString(this.start.x) + "/" + Float.toString(this.start.y),
						this.generationMode.toString()
				), Charset.forName("UTF-8")
		);
		System.out.println("Took " + (System.currentTimeMillis() - a) + "ms to create " + WORLD_DATA_FILE + " file");
		for(Chunk c : this.chunks.values()) {
			long b = System.currentTimeMillis();
			c.saveTo(directory);
			System.out.println("Took " + (System.currentTimeMillis() - b) + "ms to save chunk " + c.getX());
		}
		System.out.println("Done saving! Took " + (System.currentTimeMillis() - a) + "ms");
		System.out.println();
	}
	
	/**
	 * Loads a world and all its chunks, from a given folder.
	 * @param f The folder where all the files are saved.
	 * @return The newly loaded World.
	 * @throws IOException
	 */
	public static World loadFrom(Path f) throws IOException {
		if(!f.resolve(WORLD_DATA_FILE).toFile().exists()) {
			throw new IOException("No world data was found in the given path!");
		}
		
		Path world = f.resolve(WORLD_DATA_FILE);
		
		String worldName = "World";
		long worldSeed = new Random().nextLong();
		PVector start = null;
		int line = 0;
		Color color = null;
		GenerationMode genMode = GenerationMode.CLASSIC;
		
		for(String s : Files.readAllLines(world)) {
			switch(line) {
			case 0:
				worldName = s;
				break;
			case 1:
				worldSeed = Long.parseLong(s);
				break;
			case 2:
				color = new Color(Integer.parseInt(s));
				break;
			case 3:
				start = new PVector();
				start.x = Float.parseFloat(s.split("/")[0]);
				start.y = Float.parseFloat(s.split("/")[1]);
				break;
			case 4:
				genMode = GenerationMode.valueOf(s);
				break;
			default:
				break;
			}
			line++;
		}
		
		World w = new World(worldName, genMode, worldSeed);
		if(color != null)
			w.color = color;
		if(start != null)
			w.start = start;
		
		for(File file : f.toFile().listFiles()) {
			if(file.getName().endsWith("." + Chunk.CHUNK_EXTENSION)) {
				int index = Integer.parseInt(file.getName().replace("." + Chunk.CHUNK_EXTENSION, ""));
				try {					
					Chunk c = Chunk.loadFrom(w, index, file.toPath());
					w.chunks.put(index, c);
				} catch (IOException e) {
					System.err.println("The given chunk file (\"" + file.getName() + "\") isn't readable.");
				}
			}
		}
		System.out.println("Done loading world \"" + f.getFileName() + "\".");
		return w;
	}
	
}
