package com.n1ark.whereitends.graphics;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.particle.Particle;
import com.n1ark.whereitends.util.Keyboard;
import com.n1ark.whereitends.world.World;

public class Window extends JFrame implements KeyListener, WindowListener {
	private static final long serialVersionUID = 229077810023970255L;

	public static enum WindowState {
		PLAYING_IN_WORLD, EDITING_WORLD, WORLD_CHOOSING_MENU, WORLD_CREATING_MENU, FACE_SELECT_MENU;
	}

	/**
	 * The current FPS settings, that will be used to determine when the screen
	 * should be refreshed.
	 */
	public int FPS_SETTINGS = 60;

	/**
	 * A counter, reset to 0 every second, that should be incremented everytime a
	 * panel is repainted.
	 */
	public int CURRENT_FRAMES = 0;

	/**
	 * The current FPS rate, it is updated every second, and should be used to see
	 * the current FPS rate.
	 */
	public int FRAMES = 0;
	
	/**
	 * The window's current state. If this state isn't <code>WindowState.PLAYING</code>, the game will not tick.
	 */
	public WindowState CURRENT_STATE;

	/**
	 * The current content of the Window.
	 */
	public JPanel CURRENT_CONTENT;
	
	/**
	 * If the Window needs to render new frames.
	 */
	public boolean RENDERING;
	
	/**
	 * The index of currently selected face by the user for the Player.
	 */
	public int selectedFace;
	
	/**
	 * Starts a new Window, with the WorldSelectionMenu.
	 */
	public Window() {
		super();
		this.setTitle("Where It Ends");
		this.setSize(new Dimension(1080, 720));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationByPlatform(true);
		this.setFocusable(true);
		this.setMinimumSize(new Dimension(270, 180));
		this.addKeyListener(this);
		this.addWindowListener(this);
		new Keyboard().start(this);
		this.RENDERING = false;
		this.selectedFace = 0;
		
		switchToWorldSelectMenu();
		
		this.pack();
		this.setVisible(true);
		int timeout;
		long chrono, fpsCount = System.currentTimeMillis();
		
		// Line for testing possible fps with no delay
		//new Thread(new Runnable() {@Override public void run() {while(true) Window.this.repaint(); }}).start();
		
		while (true) {
			try {
				chrono = System.currentTimeMillis();
				if(this.RENDERING) {
					if(this.CURRENT_STATE == WindowState.PLAYING_IN_WORLD && !((WorldView) this.CURRENT_CONTENT).isPaused()) {
						WhereItEnds.INSTANCE.PLAYER.tick();
						for(Particle p : WhereItEnds.INSTANCE.PLAYER.getWorld().getParticles())
							p.tick();
					}
					repaint();
					if(System.currentTimeMillis() - fpsCount >= 1000) {
						this.FRAMES = this.CURRENT_FRAMES;
						this.CURRENT_FRAMES = 0;
						fpsCount = System.currentTimeMillis();
					}
				}
				
				try {
					timeout = (int) (Math.floorDiv(1000, this.FPS_SETTINGS) - System.currentTimeMillis() + chrono);
					Thread.sleep(timeout < 1 ? 1 : timeout);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Will change the current Window to a new WorldSelectionMenu, where the player will be able to choose
	 * a world to load.
	 */
	public void switchToWorldSelectMenu() {
		switchToWorldSelectMenu("");
	}
	
	/**
	 * Will change the current Window to a new WorldSelectionMenu, where the player will be able to choose
	 * a world to load. The currently generating world will have the specified name.
	 * @param name The name of the currently generating world.
	 */
	public void switchToWorldSelectMenu(String name) {
		this.CURRENT_CONTENT = new WorldSelectionMenu(this, name);
		this.CURRENT_STATE = WindowState.WORLD_CHOOSING_MENU;
		setupFrame();
	}
	
	/**
	 * Will change the current Window to a new WorldGenerationPrecise, where the player will be able to choose
	 * how the world it will generate will be.
	 * @param name The name of the world (it can be modified in the menu).
	 */
	public void switchToWorldGenerateMenu(String name) {
		this.CURRENT_CONTENT = new WorldGenerationPrecise(this, name);
		this.CURRENT_STATE = WindowState.WORLD_CREATING_MENU;
		setupFrame();
	}
	
	/**
	 * Will change the current Window to a new PlayerFaceSelect, where the user will be able to select
	 * their character's face.
	 */
	public void switchToPlaceFaceSelectMenu() {
		this.CURRENT_CONTENT = new PlayerFaceSelect(this);
		this.CURRENT_STATE = WindowState.FACE_SELECT_MENU;
		setupFrame();
	}
	
	/**
	 * Will change the current Window to a new WorldView, with the specified world.
	 * @param w The world to view.
	 */
	public void switchToWorldView(World w) {
		this.CURRENT_CONTENT = new WorldView(w, this);
		this.CURRENT_STATE = WindowState.PLAYING_IN_WORLD;
		WhereItEnds.INSTANCE.PLAYER.setCurrentFace(this.selectedFace);
		WhereItEnds.INSTANCE.PLAYER.setWorldView((WorldView) this.CURRENT_CONTENT);
		setupFrame();
	}
	
	/**
	 * Will change the current Window to a new WorldBuilder, with the specified world.
	 * @param w The world to edit.
	 */
	public void switchToWorldEdit(World w) {
		this.CURRENT_CONTENT = new WorldBuilder(w, this);
		this.CURRENT_STATE = WindowState.EDITING_WORLD;
		setupFrame();
	}
	
	/**
	 * Will empty the Window and add the current content.
	 */
	private void setupFrame() {
		this.getContentPane().removeAll();
		this.getContentPane().add(this.CURRENT_CONTENT);
		this.revalidate();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if(this.CURRENT_STATE == WindowState.PLAYING_IN_WORLD) 
				((WorldView) this.CURRENT_CONTENT).setPaused(!((WorldView) this.CURRENT_CONTENT).isPaused());
			else if(this.CURRENT_STATE == WindowState.EDITING_WORLD)
				((WorldBuilder) this.CURRENT_CONTENT).setPaused(!((WorldBuilder) this.CURRENT_CONTENT).isPaused());
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {/* Not used */}
	@Override
	public void keyTyped(KeyEvent e) {/* Not used */}

	@Override
	public void windowActivated(WindowEvent arg0) {
		this.RENDERING = true;
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		closing();
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		closing();
	}
	
	/**
	 * A function that will automatically save the world, if the Window is in the appropriate state.
	 */
	private void closing() {
		if(this.CURRENT_STATE == WindowState.EDITING_WORLD || this.CURRENT_STATE == WindowState.PLAYING_IN_WORLD)
			try {
				WhereItEnds.INSTANCE.WORLD.saveTo(WhereItEnds.INSTANCE.SAVE_FOLDER);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		this.RENDERING = false;
		if(this.CURRENT_STATE == WindowState.PLAYING_IN_WORLD)
			((WorldView) this.CURRENT_CONTENT).setPaused(true);
		else if(this.CURRENT_STATE == WindowState.EDITING_WORLD)
			((WorldBuilder) this.CURRENT_CONTENT).setPaused(true);
		this.CURRENT_CONTENT.repaint();
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {/* Not used */}

	@Override
	public void windowIconified(WindowEvent arg0) {/* Not used */}

	@Override
	public void windowOpened(WindowEvent arg0) {/* Not used */}
}
