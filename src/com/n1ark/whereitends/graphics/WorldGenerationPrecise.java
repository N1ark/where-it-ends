package com.n1ark.whereitends.graphics;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.component.Button;
import com.n1ark.whereitends.graphics.component.Button.ButtonListener;
import com.n1ark.whereitends.graphics.component.ButtonList;
import com.n1ark.whereitends.graphics.component.ButtonList.ButtonListListener;
import com.n1ark.whereitends.graphics.component.IconButton;
import com.n1ark.whereitends.graphics.component.IconButton.Icon;
import com.n1ark.whereitends.graphics.component.TextInput;
import com.n1ark.whereitends.graphics.component.TextInput.TextInputListener;
import com.n1ark.whereitends.graphics.component.UIComponent;
import com.n1ark.whereitends.world.GenerationMode;
import com.n1ark.whereitends.world.World;

public class WorldGenerationPrecise extends JPanel implements ButtonListener, ButtonListListener, ComponentListener, TextInputListener{
	private static final long serialVersionUID = -8477337456675962149L;
	/**
	 * The text input, where the player should write the world's name when generating it.
	 */
	private final TextInput nameInput;
	/**
	 * The seed input, where the player should write the world's seed when generating it.
	 */
	private final TextInput seedInput;
	/**
	 * The list that contains each mode of generating a terrain.
	 */
	private final ButtonList genMode;
	/**
	 * The "generate" button, that will only be displayed if the text input isn't null.
	 */
	private final Button generate;
	/**
	 * The button that will allow the Player to go back to the menu before.
	 */
	private final IconButton backButton;
	/**
	 * The ration of the background, on Width/Height.
	 */
	private final float backgroundRatio = 54f/36f;
	/**
	 * The X Position to draw the background image.
	 */
	private int backgroundX;
	/**
	 * The Y Position to draw the background image.
	 */
	private int backgroundY;
	/**
	 * The width of the background, for rendering it.
	 */
	private int backgroundWidth;
	/**
	 * The height of the background, for rendering it.
	 */
	private int backgroundHeight;
	/**
	 * This view's mother.
	 */
	private final Window mother;
	
	/**
	 * Will create a WorldGenerationPrecise, that will allow the Player to generate a world by customizing the options.
	 * @param mother The Window in which the menu will be displayed.
	 * @param name The name of the currently generating world.
	 */
	public WorldGenerationPrecise(Window mother, String name) {
		super();
		this.setPreferredSize(mother.getSize());
		this.setFocusable(true);
		this.mother = mother;
		
		List<String> worlds = new ArrayList<>();
		for(File f : WhereItEnds.INSTANCE.SAVE_FOLDER.toFile().listFiles()) {
			if(f.toPath().resolve(World.WORLD_DATA_FILE).toFile().exists()) {
				worlds.add(f.getName());
			}
		}
		String[] values = new String[worlds.size()];
		for(int i = 0; i < values.length; i++) {
			values[i] = worlds.get(i);
		}
		
		this.nameInput = new TextInput(name, "Name", this, mother, 0.5f, 0.3f);
		this.seedInput = new TextInput("", "Seed", (short) 20, this, mother, 0.5f, 0.43f);
		this.generate = new Button("Generate", this, 0.5f, 0.8f);
		this.backButton = new IconButton(this, Icon.BACK, 0.1f, 0.1f);
		// Le gros truc long en dessous permet juste de transformer direct un GenerationMode[] en String[] via leur toString[]
		this.genMode = new ButtonList(this, 0.8f, 0.5f, Arrays.stream(GenerationMode.values()).map(g -> g.toString().toLowerCase()).toArray(String[]::new));
		this.genMode.setUnselectable(false);
		
		this.nameInput.setListening(true);
		this.backButton.setListening(true);
		this.seedInput.setListening(true);
		this.genMode.setListening(true);
		
		this.generate.addButtonListener(this);
		this.backButton.addButtonListener(this);
		this.nameInput.addTextInputListener(this);
		this.genMode.addButonListListener(this);
		addComponentListener(this);
		
		inputChangedValue(this.nameInput, name);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(WorldSelectionMenu.MENU_BACKGROUND, this.backgroundX, this.backgroundY, this.backgroundWidth, this.backgroundHeight, this);
				
		this.genMode.paint(g);
		this.nameInput.paint(g);
		this.seedInput.paint(g);
		this.backButton.paint(g);
		if(this.generate.isListening())
			this.generate.paint(g);
	}

	@Override
	public void buttonPressed(UIComponent b) {
		if(b == this.generate){
			List<String> worlds = new ArrayList<>();
			for(File f : WhereItEnds.INSTANCE.SAVE_FOLDER.toFile().listFiles()) {
				if(f.toPath().resolve(World.WORLD_DATA_FILE).toFile().exists()) {
					worlds.add(f.getName());
				}
			}
			
			String name = this.nameInput.getText();
			
			while(worlds.contains(name))
				name += '-';
			
			long seed;
			if(this.seedInput.isEmpty())
				seed = new Random().nextLong();
			else try {
				seed = Long.parseLong(this.seedInput.getText());
			} catch (NumberFormatException e) {
				seed = this.seedInput.getText().hashCode();
			}
			
			GenerationMode mode = GenerationMode.valueOf(this.genMode.getCurrentValue().toUpperCase());
			
			World w = new World(name, mode, seed);
			WhereItEnds.INSTANCE.WORLD = w;
			try {
				WhereItEnds.INSTANCE.PLAYER = new Player(w);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mother.switchToWorldView(w);
		} else if (b == this.backButton) {
			this.mother.switchToWorldSelectMenu(this.nameInput.getText());
		}
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		if((double) getWidth() / getHeight() > this.backgroundRatio) {
			this.backgroundWidth = getWidth();
			this.backgroundHeight = (int) (this.backgroundWidth / this.backgroundRatio);
			this.backgroundX = 0;
			this.backgroundY = (getHeight() - this.backgroundHeight)/2;
		} else {
			this.backgroundHeight = getHeight();
			this.backgroundWidth = (int) (this.backgroundHeight * this.backgroundRatio);
			this.backgroundY = 0;
			this.backgroundX = (getWidth() - this.backgroundWidth)/2;
		}
	}

	@Override
	public void changedSelectedIndex(ButtonList bl, int index) {
		boolean worldName = !this.nameInput.isEmpty() && this.genMode.getCurrentIndex() != -1;
		this.generate.setListening(worldName);
	}
	
	@Override
	public void inputChangedValue(TextInput i, String text) {
		boolean worldName = !this.nameInput.isEmpty() && this.genMode.getCurrentIndex() != -1;
		this.generate.setListening(worldName);
	}

	@Override
	public void doubleClickedOption(ButtonList bl, int index) {/*Not used*/}

	@Override
	public void inputChangedState(TextInput i, boolean selected) {/*Not used*/}
	
	
	@Override
	public Dimension getPreferredSize() {
		return this.mother.getSize();
	}

	@Override
	public void componentHidden(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentMoved(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentShown(ComponentEvent e) {/*Not used*/}

}
