package com.n1ark.whereitends.graphics;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.component.Button;
import com.n1ark.whereitends.graphics.component.Button.ButtonListener;
import com.n1ark.whereitends.graphics.component.ButtonList;
import com.n1ark.whereitends.graphics.component.ButtonList.ButtonListListener;
import com.n1ark.whereitends.graphics.component.IconButton.Icon;
import com.n1ark.whereitends.graphics.component.IconButton;
import com.n1ark.whereitends.graphics.component.Switch;
import com.n1ark.whereitends.graphics.component.TextInput;
import com.n1ark.whereitends.graphics.component.TextInput.TextInputListener;
import com.n1ark.whereitends.graphics.component.UIComponent;
import com.n1ark.whereitends.world.GenerationMode;
import com.n1ark.whereitends.world.World;

public class WorldSelectionMenu extends JPanel implements ButtonListener, ButtonListListener, ComponentListener, TextInputListener {
	private static final long serialVersionUID = 3706979685847512575L;
	
	/**
	 * The image used for the background of the menu.
	 */
	public static Image MENU_BACKGROUND;
	/**
	 * The text input, where the player should write the world's name when generating it.
	 */
	private final TextInput nameInput;
	/**
	 * The "generate" button, that will only be displayed if the text input isn't null.
	 */
	private final Button generate;
	/**
	 * The list, that will display all currently saved worlds.
	 */
	private final ButtonList list;
	/**
	 * A switch, that is enabled by default and that indicated if the user will play (true) or edit a world (false).
	 */
	private final Switch playMode;
	/**
	 * The button that opens the window to set the new World with more settings.
	 */
	private final IconButton genOptions;
	/**
	 * The button that opens the window to select the Player's face.
	 */
	private final IconButton faceSelect;
	/**
	 * The ration of the background, on Width/Height.
	 */
	private final float backgroundRatio = 54f/36f;
	/**
	 * The X Position to draw the background image.
	 */
	private int backgroundX;
	/**
	 * The Y Position to draw the background image.
	 */
	private int backgroundY;
	/**
	 * The width of the background, for rendering it.
	 */
	private int backgroundWidth;
	/**
	 * The height of the background, for rendering it.
	 */
	private int backgroundHeight;
	/**
	 * This view's mother.
	 */
	private final Window mother;
	
	/**
	 * Will create a WorldSelectionMenu, that will allow the Player to select/create a world.
	 * @param mother The Window in which the menu will be displayed.
	 * @param name The name of the currently generating world.
	 */
	public WorldSelectionMenu(Window mother, String name) {
		super();
		this.setPreferredSize(mother.getSize());
		this.setFocusable(true);
		this.mother = mother;
		
		List<String> worlds = new ArrayList<>();
		for(File f : WhereItEnds.INSTANCE.SAVE_FOLDER.toFile().listFiles()) {
			if(f.toPath().resolve(World.WORLD_DATA_FILE).toFile().exists()) {
				worlds.add(f.getName());
			}
		}
		String[] values = new String[worlds.size()];
		for(int i = 0; i < values.length; i++) {
			values[i] = worlds.get(i);
		}
		
		this.nameInput = new TextInput(name, "Name", this, mother, 0.7f, 0.6f);
		this.list = new ButtonList(this, 0.2f, 0.5f, values);
		this.playMode = new Switch("Play", this, 0.2f, 0.9f);
		this.generate = new Button("Generate", this, 0.7f, 0.8f);
		this.genOptions = new IconButton(this, Icon.MORE, 0.88f, 0.8f);
		this.faceSelect = new IconButton(this, Icon.FACE, 0.88f, 0.2f);
		
		this.nameInput.setListening(true);
		this.list.setListening(true);
		this.genOptions.setListening(true);
		this.faceSelect.setListening(true);
		this.playMode.setEnabled(true);
		
		this.generate.addButtonListener(this);
		this.genOptions.addButtonListener(this);
		this.list.addButonListListener(this);
		this.nameInput.addTextInputListener(this);
		this.faceSelect.addButtonListener(this);
		addComponentListener(this);
		
		inputChangedValue(this.nameInput, name);
	}
	
	/**
	 * Will create a WorldSelectionMenu, that will allow the Player to select/create a world. The world name will be empty.
	 * @param mother The Window in which the menu will be displayed.
	 */
	public WorldSelectionMenu(Window mother) {
		this(mother, "");
	}
	
	/**
	 * Will load the required textures for this class.
	 * @param cl The classLoader used.
	 * @throws IOException 
	 */
	public static void load(ClassLoader cl) throws IOException {
		MENU_BACKGROUND = ImageIO.read(cl.getResourceAsStream("assets/gui/menu_background.png"));
	}
		
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(MENU_BACKGROUND, this.backgroundX, this.backgroundY, this.backgroundWidth, this.backgroundHeight, this);
				
		this.nameInput.paint(g);
		this.list.paint(g);
		this.playMode.setListening(this.list.getCurrentIndex() != -1);
		this.genOptions.paint(g);
		this.faceSelect.paint(g);
		if(this.list.getCurrentIndex() != -1)
			this.playMode.paint(g);
		if(this.generate.isListening()) {
			this.generate.paint(g);		
		}
	}

	@Override
	public void buttonPressed(UIComponent b) {
		if(b == this.generate){
			List<String> worlds = new ArrayList<>();
			for(File f : WhereItEnds.INSTANCE.SAVE_FOLDER.toFile().listFiles())
				if(f.toPath().resolve(World.WORLD_DATA_FILE).toFile().exists())
					worlds.add(f.getName());
			
			String name = this.nameInput.getText();
			
			while(worlds.contains(name))
				name += '-';
			
			World w = new World(name, GenerationMode.CLASSIC, new Random().nextLong());
			WhereItEnds.INSTANCE.WORLD = w;
			try {
				WhereItEnds.INSTANCE.PLAYER = new Player(w);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mother.switchToWorldView(w);
		} else if (b == this.genOptions) {
			this.mother.switchToWorldGenerateMenu(this.nameInput.getText());
		} else if (b == this.faceSelect) {
			this.mother.switchToPlaceFaceSelectMenu();
		}
	}
	
	@Override
	public void doubleClickedOption(ButtonList bl, int index) {
		try {
			World w = World.loadFrom(WhereItEnds.INSTANCE.SAVE_FOLDER.resolve(bl.getCurrentValue()));
			WhereItEnds.INSTANCE.WORLD = w;
			WhereItEnds.INSTANCE.PLAYER = new Player(w);
			if(this.playMode.isEnabled())
				this.mother.switchToWorldView(w);
			else
				this.mother.switchToWorldEdit(w);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		if((double) getWidth() / getHeight() > this.backgroundRatio) {
			this.backgroundWidth = getWidth();
			this.backgroundHeight = (int) (this.backgroundWidth / this.backgroundRatio);
			this.backgroundX = 0;
			this.backgroundY = (getHeight() - this.backgroundHeight)/2;
		} else {
			this.backgroundHeight = getHeight();
			this.backgroundWidth = (int) (this.backgroundHeight * this.backgroundRatio);
			this.backgroundY = 0;
			this.backgroundX = (getWidth() - this.backgroundWidth)/2;
		}
	}

	@Override
	public void inputChangedState(TextInput i, boolean selected) {/*Not used*/}
	
	@Override
	public void inputChangedValue(TextInput i, String text) {
		boolean worldName = !i.isEmpty();
		this.generate.setListening(worldName);
	}
	
	@Override
	public void changedSelectedIndex(ButtonList bl, int index) {/*Not used*/}
	
	@Override
	public Dimension getPreferredSize() {
		return this.mother.getSize();
	}

	@Override
	public void componentHidden(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentMoved(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentShown(ComponentEvent e) {/*Not used*/}
}
