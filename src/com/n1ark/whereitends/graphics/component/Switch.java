package com.n1ark.whereitends.graphics.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.n1ark.whereitends.WhereItEnds;

public class Switch extends UIComponent {
	/**
	 * The button's texture.
	 */
	protected static Image TEXTURE_ON;
	/**
	 * The button's texture when hovering.
	 */
	protected static Image TEXTURE_OFF;
	/**
	 * This button's label, that will be displayed on it.
	 */
	protected String label;
	/**
	 * The button's font's size.
	 */
	protected float fontSize;
	/**
	 * The current state of the switch. 
	 */
	protected boolean state;
	/**
	 * The list with all the ButtonListeners, that need to receive a click event.
	 */
	protected List<SwitchListener> listeners;
	
	public Switch(String label, Component mother, float x, float y) {
		super(mother, x, y);
		this.listeners = new ArrayList<>();
		this.state = false;
		this.label = label;
	}
	
	/**
	 * Will load the necessary files for this class to work.
	 * @param cl The class loader to get the files.
	 * @throws IOException
	 */
	public static void load(ClassLoader cl) throws IOException {
		TEXTURE_ON = ImageIO.read(cl.getResourceAsStream("assets/gui/switch_on.png"));
		TEXTURE_OFF = ImageIO.read(cl.getResourceAsStream("assets/gui/switch_off.png"));
	}
	
	/**
	 * @return The button's label.
	 */
	public String getLabel() {
		return this.label;
	}
	
	/**
	 * Changes the button's current label.
	 * @param label The new label.
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * @return If the switch is enabled.
	 */
	public boolean isEnabled() {
		return this.state;
	}
	
	/**
	 * Will set the state of the switch.
	 * @param enabled If it should be enabled.
	 */
	public void setEnabled(boolean enabled) {
		this.state = enabled;
	}
	
	@Override
	protected void recalculate() {
		this.width = this.mother.getWidth()/9;
		this.height = 6 * this.width / 13;
		this.xPosition = (int) (this.mother.getWidth() * this.x - this.width/2);
		this.yPosition = (int) (this.mother.getHeight() * this.y - this.height/2);
		this.fontSize = (int) (this.height * 0.7);
		Font f = WhereItEnds.NORMAL_FONT.deriveFont(this.fontSize);
		while(this.mother.getGraphics().getFontMetrics(f).stringWidth(this.label) >= this.width * 0.7) {
			this.fontSize -= 1;
			f = f.deriveFont(this.fontSize);
		}
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawImage(this.state ? TEXTURE_ON: TEXTURE_OFF, this.xPosition, this.yPosition, this.width, this.height, this.mother);		

		g.setFont(WhereItEnds.NORMAL_FONT.deriveFont(this.fontSize));
		int txtW = g.getFontMetrics().stringWidth(this.label);
		
		g.setColor(Color.BLACK);
		g.drawString(this.label, this.xPosition + (this.width - txtW)/2, (int) (this.yPosition + (3*this.height + 2 * this.fontSize)/6));
	}
	
	/**
	 * Adds a Listener to this button, that will have its <code>buttonPressed</code> method triggered when this
	 * button is pressed. This is triggered with the mouse's left click release.
	 * @param b The listener to add.
	 */
	public void addButtonListener(SwitchListener b) {
		this.listeners.add(b);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(this.listening) {
			if(
					e.getX() >= this.xPosition && 
					e.getX() < this.xPosition + this.width && 
					e.getY() >= this.yPosition && 
					e.getY() < this.yPosition + this.height && 
					e.getButton() == MouseEvent.BUTTON1
			) {
				this.state = !this.state;
				for(SwitchListener sl : this.listeners)
					sl.stateChanged(this);
			}
		}
	}
	
	public static interface SwitchListener {
		/**
		 * Invoked whenever the switched is pressed.
		 * @param b The pressed switch.
		 */
		void stateChanged(Switch s);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {/* Not used */}
	@Override
	public void mouseEntered(MouseEvent e) {/* Not used */}
	@Override
	public void mouseExited(MouseEvent e) {/* Not used */}
	@Override
	public void mousePressed(MouseEvent e) {/* Not used */}
	@Override
	public void mouseDragged(MouseEvent e) {/* Not used */}
	@Override
	public void mouseMoved(MouseEvent e) {/* Not used */}
	@Override
	public void componentHidden(ComponentEvent e) {/* Not used */}
	@Override
	public void componentMoved(ComponentEvent e) {/* Not used */}
	@Override
	public void componentShown(ComponentEvent e) {/* Not used */}	
}
