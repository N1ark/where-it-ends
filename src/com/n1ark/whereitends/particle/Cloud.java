package com.n1ark.whereitends.particle;

import java.awt.Color;
import java.util.Random;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.world.World;

public class Cloud extends Particle{

	/**
	 * By how much should the rotation by increased when ticking.
	 */
	private final float rotInc;
	/**
	 * The particle's color.
	 */
	private final Color color;
	
	public Cloud(World w, float x, float y) {
		super(w, x, y);
		Random r = WhereItEnds.random;
		this.life = 180 + r.nextInt(30);
		this.rotInc = (r.nextBoolean() ? 1 : -1) * (r.nextFloat()*0.3f+1.1f)/this.life;
		this.rotation = r.nextFloat()*3.14f;
		int hue = 180 + r.nextInt(70);
		this.color = new Color(hue,hue,hue);
		this.width = 0.7f + r.nextFloat()*0.3f;
		this.height = 0.5f + r.nextFloat()*0.2f;
	}

	@Override
	public Color getColor() {
		return this.color;
	}
	
	@Override
	public void tick() {
		super.tick();
		this.rotation += this.rotInc;
		this.width *= 0.993f;
		this.height *= 0.993f;
	}
}
