package com.n1ark.whereitends.entity;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.graphics.WorldView;
import com.n1ark.whereitends.particle.Cloud;
import com.n1ark.whereitends.particle.Spark;
import com.n1ark.whereitends.util.Keyboard;
import com.n1ark.whereitends.util.PVector;
import com.n1ark.whereitends.util.Util;
import com.n1ark.whereitends.world.Block;
import com.n1ark.whereitends.world.Effect;
import com.n1ark.whereitends.world.World;

public class Player extends Entity {
	
	/**
	 * The gravity vector, to be added every tick.
	 */
	private static final PVector GRAVITY = new PVector(0, -0.03f);

	/**
	 * The image that has to be displayed for the skin. To use it, display the image
	 * for every width.
	 */
	public static Image SKIN;
	/**
	 * The image that has to be displayed for the skin when the Player has a double jump effect.
	 */
	public static Image SKIN_DOUBLE_JUMP;
	/**
	 * The image that has to be displayed for the skin when the Player has a bonus size effect.
	 */
	public static Image SKIN_SIZE_PLUS;
	/**
	 * The list of faces that can be used for the Player.
	 */
	public static Image FACES[];
	/**
	 * The image that has to be displayed for the face. To use it, place it in the middle
	 * of the body.
	 */
	private Image currentFace;
	/**
	 * If the player is currently on solid ground.
	 */
	private boolean onGround;
	/**
	 * If the player is currently crouching.
	 */
	private boolean crouching;
	/**
	 * The amount of coins the Player caught.
	 */
	private int coinsTaken;
	/**
	 * The current Effect the Player is under.
	 */
	private Effect currentEffect;
	/**
	 * The WorldView, that needs to receive updates on any change of with or height from the player.
	 */
	private WorldView worldView;
	/**
	 * The max size (width or height) this Player can have.
	 */
	private float maxSize;
	/**
	 * The minimum size (width or height) this Player can have.
	 */
	private float minSize;
	/**
	 * The frames left before the Player loses its effect.
	 */
	private int effectTimer;
	/**
	 * The max amount of frames the timer had.
	 */
	private int maxTimer;
	
	/**
	 * Create a player at {0;60} coordinates, in a world.
	 * @param w The world where the player is spawning.
	 * @throws IOException 
	 */
	public Player(World w) throws IOException {
		this.world = w;
		this.loc = w.getSpawnLocation();
		this.onGround = false;
		this.crouching = true;
		this.vel = new PVector();
		this.maxSize = 10;
		this.minSize = 1;
		this.height = this.minSize;
		this.width = this.minSize;
		this.coinsTaken = 0;
		this.currentEffect = Effect.NONE;
		this.currentFace = FACES[0];
	}
	
	Block[] blocks;
	int bx;
	int bxSize;
	int byDown;
	int byUp;
	
	/**
	 * Will load the necessary textures for the Player.
	 * @param cl The ClassLoader for the textures.
	 * @throws IOException 
	 */
	public static void load(ClassLoader cl) throws IOException {
		SKIN = ImageIO.read(cl.getResourceAsStream("assets/character/body.png"));
		SKIN_DOUBLE_JUMP = ImageIO.read(cl.getResourceAsStream("assets/character/body_jump.png"));
		SKIN_SIZE_PLUS = ImageIO.read(cl.getResourceAsStream("assets/character/body_size.png"));
		int i = 1;
		boolean failed = false;
		List<Image> faces = new ArrayList<>();
		while(!failed) {
			try {
				faces.add(ImageIO.read(cl.getResourceAsStream("assets/character/face_" + i + ".png")));
				i++;
			} catch (Exception e) {
				failed = true;
			}
		}
		FACES = faces.toArray(new Image[0]);
	}
	
	@Override
	public void tick() {
		// Controls
		this.crouching = Keyboard.isPressed(KeyEvent.VK_SHIFT);
		if(this.onGround && Keyboard.isPressed(KeyEvent.VK_SPACE))
			jump();
		else if(this.currentEffect == Effect.DOUBLE_JUMP && Keyboard.wasPressed(KeyEvent.VK_SPACE)) {
			jump();
			this.currentEffect = Effect.NONE;
			for(int i = 0; i < this.width + 3 + WhereItEnds.random.nextInt(3); i++) {
				this.world.addParticle(new Cloud(
						this.world, 
						this.loc.x - this.width*0.8f + WhereItEnds.random.nextFloat()*this.width*1.6f, 
						this.loc.y - this.height/2 - 0.2f + WhereItEnds.random.nextFloat()*0.4f
				));
			}
		}
		if(Keyboard.isPressed(KeyEvent.VK_D))
			this.vel.x += this.crouching ? 0.01 : 0.03;
		if(Keyboard.isPressed(KeyEvent.VK_A))
			this.vel.x -= this.crouching ? 0.01 : 0.03;
		this.vel.x *= 0.8f;
		if(Math.abs(this.vel.x) < 0.001)
			this.vel.x = 0;
		this.vel.limit(0.7f);

		// Setup
		this.bx = (int) Math.floor(this.loc.x-this.width/2f);
		this.bxSize = (int) (Math.ceil(this.loc.x + this.width/2) - Math.floor(this.loc.x - this.width/2));
		this.byDown = (int) Math.ceil(this.loc.y + this.vel.y - this.height/2f);
		this.byUp = (int) Math.ceil (this.loc.y + this.vel.y + this.height/2f);
		this.blocks = new Block[this.bxSize];

		for(int i = 0; i < this.blocks.length; i++)
			this.blocks[i] = this.world.get(this.bx + i, this.byDown);
		
		this.onGround = false;
		for(Block b : this.blocks)
			if(b.isSolid()) {
				this.onGround = true;
				break;
			}
		
		// Y Collision
		if(this.vel.y > 0) { // Si on "monte"
			this.onGround = false;
			if(isUnderBlock()) {
				this.vel.y = 0;
				this.loc.y = this.byUp - 1 - this.height/2; // On le fait toucher le bloc du dessus, mais sans qu'il depasse!
				this.byUp = (int) Math.ceil(this.loc.y + this.height/2f);
			}
		
		} else if (this.vel.y < 0) { // Si on "tombe"
			if(this.onGround) {
				this.vel.y = 0;
				this.loc.y = this.byDown + this.height/2; // On le fait toucher le bloc en dessous, mais sans qu'il depasse!
				this.byUp = (int) Math.ceil(this.loc.y + this.height/2f);
			}
		}
		
		if(this.onGround)
			this.vel.y = 0;
		else {
			this.loc.y += this.vel.y;
			this.vel.add(GRAVITY);
		}
		
		// X Collision
		if(this.vel.x != 0) {
			boolean canPass = !isTouchingWall(this.vel.x > 0);

			if(canPass)
				this.loc.add(this.vel.x, 0);
			else {
				byte sign = (byte) (this.vel.x > 0 ? 1 : -1);
				int bvx = (int) Math.floor(this.loc.x + this.vel.x + sign*this.width/2);
				this.loc.x = bvx - sign * this.width/2;
				if(this.vel.x < 0)
					this.loc.x += 1;
				this.vel.x = 0;
			}
		}
		
		
		// Change Size?
		if(Keyboard.isPressed(KeyEvent.VK_1))
			increaseHeight();
		else if(Keyboard.isPressed(KeyEvent.VK_2))
			decreaseHeight();
		
		if(Keyboard.isPressed(KeyEvent.VK_3))
			increaseWidth();
		else if(Keyboard.isPressed(KeyEvent.VK_4))
			decreaseWidth();
		
		this.height = Math.round(this.height * 10f) / 10f;
		this.width = Math.round(this.width * 10f) / 10f;
		
		if(this.loc.y < 0)
			died();
		
		if(this.effectTimer > 0) {
			this.effectTimer --;
			if(this.effectTimer == 0) {
				if(this.currentEffect == Effect.SIZE_PLUS) {
					this.minSize = 1;
					this.maxSize = 10;
				}
				this.currentEffect = Effect.NONE;
			}
		}
		
		if(this.height < this.minSize)
			increaseHeight();
		else if(this.height > this.maxSize)
			decreaseHeight();
		
		if(this.width < this.minSize)
			increaseWidth();
		else if(this.width > this.maxSize)
			decreaseWidth();
		
		boolean water = false;
		for(int x = (int) Math.floor(this.loc.x - this.width/2f); x < (int) Math.ceil(this.loc.x + this.width/2f); x++)
			for(int y = (int) Math.ceil(this.loc.y - this.height/2f); y < (int) Math.ceil(this.loc.y + this.height/2f)+1; y++)
				switch(this.world.get(x, y)) {
				case BLADE:
					died();
					break;
				case JUMPER:
					this.vel.y += 0.6;
					break;
				case COIN:
					if(this.coinsTaken < 999)
						this.coinsTaken ++;
					this.world.set(Block.COIN_TAKEN, x, y);
					this.worldView.changedCoinCount(this.coinsTaken);
					for(int i = 0; i < 5+WhereItEnds.random.nextInt(3);i++) {
						this.world.addParticle(new Spark(this.world, x+.5f, y+.5f, (float) (WhereItEnds.random.nextFloat()*Math.PI*2)));
					}
					break;
				case JUMP_POTION:
					if(this.currentEffect != Effect.DOUBLE_JUMP) {
						this.world.set(Block.JUMP_POTION_TAKEN, x, y);
						if(this.currentEffect == Effect.SIZE_PLUS) {							
							this.minSize = 1;
							this.maxSize = 10;
							this.effectTimer = 0;
							this.worldView.changedCharacterHeight(this.height);
							this.worldView.changedCharacterWidth(this.width);
						}
						this.currentEffect = Effect.DOUBLE_JUMP;
					}
					break;
				case SIZE_POTION:
					if(this.currentEffect != Effect.SIZE_PLUS) {
						this.currentEffect = Effect.SIZE_PLUS;
						this.maxSize = 15;
						this.minSize = 0.5f;
						this.worldView.changedCharacterHeight(this.height);
						this.worldView.changedCharacterWidth(this.width);
						this.world.set(Block.SIZE_POTION_TAKEN, x, y);
						this.effectTimer = 420;
						this.maxTimer = this.effectTimer;
					}
					break;
				case WATER:
					if(!water) {
						water = true;
						this.vel.x *= 0.75;
						this.vel.y *= 0.75;
						if(Keyboard.isPressed(KeyEvent.VK_SPACE))
							this.vel.y += (this.crouching ? 0.035 : 0.07)/Util.map(this.width*this.height, 1, 100, 1, 3.2);
					}
					break;
				default:
					break;
				}
	}
	
	@Override
	public void died() {
		this.vel.set(0, 0);
		this.loc.set(this.world.getSpawnLocation());
		this.width = 1;
		this.height = 1;
		this.currentEffect = Effect.NONE;
		this.worldView.changedCharacterHeight(this.height);
		this.worldView.changedCharacterWidth(this.width);
	}
	
	/**
	 * @return Wether or not the player is currently on solid ground.
	 */
	public boolean isOnGround() {
		return this.onGround;
	}
	
	/**
	 * @return The amount of coins the Player caught.
	 */
	public int getCoinsTaken() {
		return this.coinsTaken;
	}
	
	/**
	 * @return The current effect of the Player.
	 */
	public Effect getCurrentEffect() {
		return this.currentEffect;
	}
	
	/**
	 * @return The current texture for the Player's body.
	 */
	public Image getBodyTexture() {
		switch(this.currentEffect) {
		case NONE: return SKIN;
		case DOUBLE_JUMP: return SKIN_DOUBLE_JUMP;
		case SIZE_PLUS: return SKIN_SIZE_PLUS;
		}
		return null;
	}
	
	/**
	 * @return The image that has to be displayed for the face. To use it, place it in the middle
	 * of the body.
	 */
	public Image getCurrentFace() {
		return this.currentFace;
	}
	
	public void setCurrentFace(int index) {
		if(index > -1 && index < FACES.length)
			this.currentFace = FACES[index];
		else
			System.err.println("Tried setting a face that doesn't exist! (Index " + index + ")");
	}
	
	/**
	 * @return The minimum size in blocks the Player can have in width/height.
	 */
	public float getMinSize() {
		return this.minSize;
	}
	
	/**
	 * @return The maximum size in blocks the Player can have in width/height.
	 */
	public float getMaxSize() {
		return this.maxSize;
	}
	
	/**
	 * @return The frames left for the current effect. Will be none if the player has no effect or it doesn't have a timer.
	 */
	public int getEffectTimer() {
		return this.effectTimer;
	}
	
	/**
	 * @return The max value for the value of effect timer. Will be equal to the max timer for the last taken potion, or null if none was taken.
	 */
	public int getMaxEffectTimer() {
		return this.maxTimer;
	}
	
	/**
	 * @return If there are any blocks directly above the player.
	 */
	public boolean isUnderBlock() {
		for(int i = 0; i < this.bxSize; i++)
			if(this.world.get(this.bx + i, this.byUp).isSolid())
				return true;
		return false;
	}
	
	/**
	 * @param rightSide If we're looking at his right or left side.
	 * @return If the player is directly touching a block next to him.
	 */
	public boolean isTouchingWall(boolean rightSide) {
		int bySize = (int) (Math.ceil(this.loc.y + this.height/2) - Math.floor(this.loc.y - this.height/2));
		int bvx;
		if(rightSide)
			bvx = (int) Math.floor(this.loc.x + this.vel.x + this.width/2);
		else
			bvx = (int) Math.ceil(this.loc.x + this.vel.x - this.width/2 - 1);
		
		for(int y = 0; y < bySize; y++)
			if(this.world.get(bvx, this.byUp - y).isSolid())
				return true;
		
		return false;
	}
	
	/**
	 * @param rightSide If we're looking at his right or left side.
	 * @return If the player is inside a block next to him.
	 */
	public boolean isInsideWall(boolean rightSide) {
		int bySize = (int) (Math.ceil(this.loc.y + this.height/2) - Math.floor(this.loc.y - this.height/2));
		int bvx = (int) (rightSide ? Math.ceil(this.loc.x + this.vel.x + this.width/2 - 1) : Math.floor(this.loc.x + this.vel.x - this.width/2));

		for(int y = 0; y < bySize; y++)
			if(this.world.get(bvx, this.byUp - y).isSolid())
				return true;
		
		return false;
	}
	
	/**
	 * Will make the Player jump.
	 */
	public void jump() {
		this.vel.y = (this.crouching ? 0.5f : 0.8f)/Util.map(this.width*this.height, 1, 100, 1, 3.2f);
	}
	
	/**
	 * Will set the WorldView, to which updates on the size of the character should be sent.
	 * @param wv The world view.
	 */
	public void setWorldView(WorldView wv) {
		this.worldView = wv;
	}
	
	/**
	 * Will try to increase the Player's width by .1, and move him according to the surrounding blocks.
	 * @return If it successfully increased the width.
	 */
	public boolean increaseWidth() {
		if(this.width < this.maxSize) {
			boolean r = isTouchingWall(true);
			boolean l = isTouchingWall(false);
			if(!r || !l) {
				if(l) { // Touche le mur gauche.
					this.loc.x += 0.1f;
					this.width += 0.1f;
					this.bx = (int) Math.floor(this.loc.x-this.width/2f);
					this.bxSize = (int) (Math.ceil(this.loc.x + this.width/2) - Math.floor(this.loc.x - this.width/2));
					if(isInsideWall(true) || isInsideWall(false)) {
						this.loc.x -= 0.1f;
						this.width -= 0.1f;
						return false;
					}
					this.worldView.changedCharacterWidth(this.width);
					return true;
				} else if(r) { // Touche le mur droit.
					this.loc.x -= 0.1f;
					this.width += 0.1f;
					this.bx = (int) Math.floor(this.loc.x-this.width/2f);
					this.bxSize = (int) (Math.ceil(this.loc.x + this.width/2) - Math.floor(this.loc.x - this.width/2));
					if(isInsideWall(true) || isInsideWall(false)) {
						this.loc.x += 0.1f;
						this.width -= 0.1f;
						return false;
					}
					this.worldView.changedCharacterWidth(this.width);
					return true;
				} else {
					this.width += 0.1f;
					this.bx = (int) Math.floor(this.loc.x-this.width/2f);
					this.bxSize = (int) (Math.ceil(this.loc.x + this.width/2) - Math.floor(this.loc.x - this.width/2));
					this.worldView.changedCharacterWidth(this.width);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Will try and decrease by .1 the width of the Player, if they can.
	 * @return If it successfully decreased the width.
	 */
	public boolean decreaseWidth() {
		if(this.width > this.minSize) {
			this.width -= 0.1f;
			this.worldView.changedCharacterWidth(this.width);
			return true;
		}
		return false;
	}
	
	/**
	 * Will try and increase by .1 the height of the Player, if they can.
	 * @return If it successfully increased the height.
	 */
	public boolean increaseHeight() {
		if(this.height < this.maxSize) {
			this.height += 0.1f;
			this.loc.y += 0.05001f;
			this.byUp = (int) Math.ceil (this.loc.y + this.vel.y + this.height/2f);
			this.byDown = (int) Math.ceil(this.loc.y + this.vel.y - this.height/2f);
			if(isUnderBlock()) {
				this.height -= 0.1f;
				this.loc.y -= 0.05f;
				return false;
			}
			this.worldView.changedCharacterHeight(this.height);
			return true;
		}
		return false;
		
	}
	
	/**
	 * Will try and decrease the Player's height, if they can.
	 * @return If it successfully decreased the height.
	 */
	public boolean decreaseHeight() {
		if(this.height > this.minSize) {
			this.byDown = (int) Math.ceil(this.loc.y + this.vel.y - this.height/2f);
			this.byUp = (int) Math.ceil (this.loc.y + this.vel.y + this.height/2f);
			this.height -= 0.1f;
			this.loc.y -= 0.0499f;
			this.worldView.changedCharacterHeight(this.height);
			return true;
		}
		return false;
	}
}
