package com.n1ark.whereitends.util;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ConcurrentHashMap;

public class Keyboard implements KeyListener{
	
	/**
	 * The HashMap containing, for each key ID, if it is currently being pressed.
	 */
	private static final ConcurrentHashMap<Integer, Boolean> keys = new ConcurrentHashMap<>();
	
	/**
	 * The HashMap containing, for each key ID, if it is currently enabled. A key switches between an enabled and
	 * disabled state whenever it is pressed.
	 */
	private static final ConcurrentHashMap<Integer, Boolean> keyEnabled = new ConcurrentHashMap<>();
	
	/**
	 * The HashMapContaining, for each key ID, if it was released. Once the value is read, it is set back to false.
	 */
	private static final ConcurrentHashMap<Integer, Boolean> keyPressed = new ConcurrentHashMap<>();

	/**
	 * Starts updating the key values, by listening to the component's KeyListener.
	 * @param component The component that will be listened to.
	 */
	public void start(Component component) {
		component.addKeyListener(this);
	}

	@Override
	public void keyTyped(KeyEvent e) {/* unused */}

	@Override
	public void keyPressed(KeyEvent e) {
		keys.put(e.getKeyCode(), true);
		keyPressed.put(e.getKeyCode(), true);
		try {
			keyEnabled.put(e.getKeyCode(), !keyEnabled.get(e.getKeyCode()));
		} catch (NullPointerException n) {
			keyEnabled.put(e.getKeyCode(), true);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys.put(e.getKeyCode(), false);
		keyPressed.put(e.getKeyCode(), false);
	}

	/**
	 * @param key The ID of the key to check.
	 * @return Wether or not the key with the corresponding ID is currently being pressed.
	 */
	public static boolean isPressed(int key) {
		if (!keys.containsKey(key))
			keys.put(key, false);
		keyPressed.put(key, false);
		return keys.get(key);
	}

	/**
	 * @param key The ID of the key to check.
	 * @return Wether or not the key with the corresponding ID is currently enabled. A key switches between
	 * enabled and disabled whenever it is pressed.
	 */
	public static boolean isEnabled(int key) {
		if (!keyEnabled.containsKey(key))
			return false;
		return keyEnabled.get(key);
	}
	
	/**
	 * @param key The ID of the key to check.
	 * @return Wether or not the key with the corresponding ID was pressed since the last check.
	 */
	public static boolean wasPressed(int key) {
		boolean pressed = keyPressed.get(key);
		keyPressed.put(key, false);
		return pressed;
	}
	
	/**
	 * Will change the enabled state of the key corresponding to the given ID to the given boolean value.
	 * @param key The ID of the key to change.
	 * @param b If the key is now enabled or not.
	 */
	public static void setDisabling(int key, boolean b) {
		keyEnabled.put(key, b);
	}
}
