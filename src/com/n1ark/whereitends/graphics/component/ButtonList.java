package com.n1ark.whereitends.graphics.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.n1ark.whereitends.WhereItEnds;

public class ButtonList extends UIComponent implements MouseWheelListener{
	
	/**
	 * The texture to be used, for a non-selected index.
	 */
	protected static Image TEXTURE;
	/**
	 * The texture to be used when an index is selected.
	 */
	protected static Image TEXTURE_SELECTED;
	/**
	 * The texture for the scroller, placed on the right side of the list.
	 */
	protected static Image SCROLLER;

	/**
	 * The values that need to be displayed in the list.
	 */
	protected String[] values;
	/**
	 * The font sizes for each index.
	 */
	protected float[] fontSizes;
	/**
	 * The size of the text for each value, with the porper font size.
	 */
	protected int[] textWidths;
	/**
	 * The last time the user clicked an element from the list. This will be used to then find a double click.
	 */
	protected long lastClick;
	/**
	 * The index of the currently selected item. If nothing is selected, this will be -1.
	 */
	protected int selectedIndex;
	/**
	 * A float that ranges from 0 to <code>values.length - 5</code>, that tells what is the index of the value at the top of the bar.
	 * This won't work if the list has less than 5 elements.
	 */
	protected float scrolling;
	/**
	 * A float, that will represent a pixel, scaled properly, for the buttons.
	 */
	protected float pixel;
	/**
	 * The X coordinate of the scroller.
	 */
	protected int scrollX;
	/**
	 * The Y coordinate of the scroller.
	 */
	protected int scrollY;
	/**
	 * The entire height of the ButtonList. <code>height</code> will result from this value, divided by 5.
	 */
	protected int fullHeight;
	/**
	 * If the user is currently pressing the scroll. This might be true even if the mouse isn't hovering the scroller.
	 */
	protected boolean pressing;
	/**
	 * The list containing all the Listeners, waiting for updates from this ButtonList.
	 */
	protected List<ButtonListListener> listeners;
	/**
	 * If this ButtonList can not be selected, which means that no button is pressed.
	 */
	protected boolean canBeUnselected;

	public ButtonList(Component mother, float x, float y, String... values) {
		super(mother, x, y);
		this.values = values;
		this.fontSizes = new float[values.length];
		this.textWidths = new int[values.length];
		this.selectedIndex = -1;
		this.lastClick = 0;
		this.scrolling = 0;
		this.listeners = new ArrayList<>();
		this.pressing = false;
		this.canBeUnselected = true;
		mother.addMouseWheelListener(this);
	}
	
	/**
	 * Will load the necessary files for this class to work.
	 * @param cl The class loader to get the files.
	 * @throws IOException
	 */
	public static void load(ClassLoader cl) throws IOException {
		TEXTURE = ImageIO.read(cl.getResourceAsStream("assets/gui/button_list.png"));
		TEXTURE_SELECTED = ImageIO.read(cl.getResourceAsStream("assets/gui/button_list_selected.png"));
		SCROLLER = ImageIO.read(cl.getResourceAsStream("assets/gui/scroller.png"));
	}
	
	/**
	 * Will add the given Listener to the listener list.
	 * @param bll The ButonListListener that will listen for updates from this ButtonList.
	 */
	public void addButonListListener(ButtonListListener bll) {
		this.listeners.add(bll);
	}
	
	/*
	 * @return The String values for the list.
	 */
	public String[] getValues() {
		return this.values;
	}
	
	/**
	 * @return The currently selected index. It will be -1 if nothing is selected
	 */
	public int getCurrentIndex() {
		return this.selectedIndex;
	}
	
	/**
	 * @return The currently selected String value. This will be null, it nothing is selected.
	 */
	public String getCurrentValue() {
		if(this.selectedIndex == -1)
			return null;
		return this.values[this.selectedIndex];
	}
	
	/**
	 * @return If this ButtonList can be unselected, which means that a Button should always be pressed.
	 */
	public boolean canBeUnselected() {
		return this.canBeUnselected;
	}
	
	/**
	 * Will set wether or not the ButtonList can be unselected, which means that a Button should always be pressed.
	 * @param canBeUnselected
	 */
	public void setUnselectable(boolean canBeUnselected) {
		this.canBeUnselected = canBeUnselected;
		if(!canBeUnselected && this.selectedIndex == -1)
			this.selectedIndex = 0;
	}
	
	@Override
	protected void recalculate() {
		this.fullHeight = (int) (this.mother.getHeight()/1.5);
		this.height = (int) (this.fullHeight/5.0);
		this.width = (int) (22f * this.height / 9f);
		this.pixel = this.width/22f;
		this.xPosition = (int) (this.mother.getWidth() * this.x - this.width/2);
		this.yPosition = (int) (this.mother.getHeight() * this.y - this.fullHeight/2.0);
		for(int i = 0; i < this.fontSizes.length; i++) {
			this.fontSizes[i] = (int) (this.height * 0.8);
			Font f = WhereItEnds.NORMAL_FONT.deriveFont(this.fontSizes[i]);
			this.textWidths[i] = this.mother.getGraphics().getFontMetrics(f).stringWidth(this.values[i]);
			while(this.textWidths[i] >= this.width * 0.7) {
				this.fontSizes[i] -= 1;
				f = f.deriveFont(this.fontSizes[i]);
				this.textWidths[i] = this.mother.getGraphics().getFontMetrics(f).stringWidth(this.values[i]);
			}
		}
	}
	

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(this.xPosition, this.yPosition, this.width, this.height * 5);
		for(int i = 0; i < 5; i++) {
			int index = Math.round(this.scrolling) + i;
			if(index >= this.values.length)
				break;
			
			g.drawImage(index == this.selectedIndex ? TEXTURE_SELECTED : TEXTURE, this.xPosition, this.yPosition + i * this.height, this.width, this.height, this.mother);
		
			g.setFont(WhereItEnds.NORMAL_FONT.deriveFont(this.fontSizes[index]));
			g.setColor(Color.BLACK);
			g.drawString(this.values[index], this.xPosition + (this.width - this.textWidths[index])/2, (int) (this.yPosition + i * this.height + (3*this.height + 2 * this.fontSizes[index])/6));
		}
		
		if(this.values.length > 5) {
			g.setColor(Color.DARK_GRAY);
			g.fillRect((int) (this.xPosition + this.width + this.pixel * 2), this.yPosition, (int) this.pixel, this.height*5);
			this.scrollX = (int) (this.xPosition + this.width + this.pixel);
			this.scrollY = (int) (this.yPosition + this.fullHeight * (this.scrolling/(this.values.length-5)) - this.pixel * 2.5);
			g.drawImage(SCROLLER, this.scrollX, this.scrollY, (int) (this.pixel * 3), (int) (this.pixel * 5), this.mother);
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(this.listening) {
			this.pressing = false;
			
			if(
					e.getX() >= this.xPosition &&
					e.getX() < this.xPosition + this.width && 
					e.getY() >= this.yPosition &&
					e.getY() < this.yPosition + this.fullHeight
			) {
				int ind =  (e.getY() - this.yPosition)/this.height;
				ind = (int) (Math.floor(this.scrolling)) + ind;
				if(ind < this.values.length){
					if(System.currentTimeMillis() - this.lastClick < 500) {
						for(ButtonListListener bll : this.listeners)
							bll.doubleClickedOption(this, ind);
						this.lastClick = 0;
					} else
						this.lastClick = System.currentTimeMillis();
				}
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(this.listening) {
			this.pressing =
					e.getX() >= this.scrollX &&
					e.getX() < this.scrollX + this.pixel * 3 &&
					e.getY() >= this.scrollY &&
					e.getY() < this.scrollY + this.pixel * 5;
					
			if(
					e.getX() >= this.xPosition &&
					e.getX() < this.xPosition + this.width && 
					e.getY() >= this.yPosition &&
					e.getY() < this.yPosition + this.fullHeight
			) {
				int ind =  (e.getY() - this.yPosition)/this.height + (int) (Math.floor(this.scrolling));
				if(ind >= this.values.length) {
					ind = -1;
					this.lastClick = 0;
				}
				
				if(ind != this.selectedIndex)
					this.lastClick = 0;
				
				if (ind != -1 || this.canBeUnselected)
					this.selectedIndex = ind;
				
				for(ButtonListListener bll : this.listeners)
					bll.changedSelectedIndex(this, this.selectedIndex);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(this.listening && this.pressing && this.values.length > 5) {
			this.scrolling = ((float) (e.getY() - this.yPosition)) / (this.height * 5) * (this.values.length - 5);
			if(this.scrolling < 0)
				this.scrolling = 0;
			if(this.scrolling > this.values.length - 5)
				this.scrolling = this.values.length - 5;
		}
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(this.listening) {
			if(this.values.length > 5) {
				this.scrolling += e.getWheelRotation();
				if(this.scrolling < 0)
					this.scrolling = 0;
				if(this.scrolling > this.values.length - 5)
					this.scrolling = this.values.length - 5;
			}
		}
	}
	
	public static interface ButtonListListener {
		void changedSelectedIndex(ButtonList bl, int index);
		void doubleClickedOption(ButtonList bl, int index);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {/* Not used */}
	@Override
	public void mouseClicked(MouseEvent e) {/* Not used */}
	@Override
	public void mouseEntered(MouseEvent e) {/* Not used */}
	@Override
	public void mouseExited(MouseEvent e) {/* Not used */}
	@Override
	public void componentHidden(ComponentEvent e) {/* Not used */}
	@Override
	public void componentMoved(ComponentEvent e) {/* Not used */}
	@Override
	public void componentShown(ComponentEvent e) {/* Not used */}
}
