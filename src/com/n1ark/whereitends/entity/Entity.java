package com.n1ark.whereitends.entity;

import com.n1ark.whereitends.util.PVector;
import com.n1ark.whereitends.world.World;

public abstract class Entity {
	/**
	 * The entity height.
	 */
	protected float height;
	/**
	 * The entity's width.
	 */
	protected float width;
	/**
	 * The entity's position.
	 */
	protected PVector loc;
	/**
	 * The entity's velocity.
	 */
	protected PVector vel;
	/**
	 * The entity's current world.
	 */
	protected World world;
	
	/**
	 * Will do all the movement for the entity, as well as anything else needed.
	 */
	public abstract void tick();
	
	/**
	 * A method that needs to be called whenever an entity should be killed.
	 */
	public abstract void died();
	
	/**
	 * @return The height of the entity.
	 */
	public float getHeight() {
		return this.height;
	}
	/**
	 * @return The width of the entity.
	 */
	public float getWidth() {
		return this.width;
	}
	/**
	 * @return The current location of the entity.
	 */
	public PVector getLocation() {
		return this.loc;
	}
	/**
	 * @return The current velocity of the entity.
	 */
	public PVector getVelocity() {
		return this.vel;
	}
	/**
	 * @return The current world in which the entity is.
	 */
	public World getWorld() {
		return this.world;
	}
	/**
	 * Sets the entity current world.
	 * @param w The world where it now is.
	 */
	public void setWorld(World w) {
		this.world.getEntities().remove(this);
		this.world = w;
		this.world.getEntities().add(this);
	}
}
