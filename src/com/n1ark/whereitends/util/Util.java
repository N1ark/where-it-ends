package com.n1ark.whereitends.util;

public class Util {
	/**
	 * Will map the given value, so it will proportionally fit between the first scale (a), and the second one (b), where
	 * <code>map(am, am, aM, bm, bM) = bm</code> and <code>map(aM, am, aM, bm, bM) = bM </code>
	 * @param value The value to map.
	 * @param aMin The minimum value on the first scale.
	 * @param aMax The maximum value on the first scale.
	 * @param bMin The minimum value on the second scale.
	 * @param bMax The maximum value on the second scale.
	 * @return The equivalent of the value in the first scale, in the second scale.
	 */
	public static double map(double value, double aMin, double aMax, double bMin, double bMax) {
		return bMin + (value-aMin)*((bMax-bMin)/(aMax-aMin));
	}
	
	/**
	 * Will map the given value, so it will proportionally fit between the first scale (a), and the second one (b), where
	 * <code>map(am, am, aM, bm, bM) = bm</code> and <code>map(aM, am, aM, bm, bM) = bM </code>
	 * @param value The value to map.
	 * @param aMin The minimum value on the first scale.
	 * @param aMax The maximum value on the first scale.
	 * @param bMin The minimum value on the second scale.
	 * @param bMax The maximum value on the second scale.
	 * @return The equivalent of the value in the first scale, in the second scale.
	 */
	public static float map(float value, float aMin, float aMax, float bMin, float bMax) {
		return bMin + (value-aMin)*((bMax-bMin)/(aMax-aMin));
	}
	
	/**
	 * Returns the index of the first occurrence of the specified elementin this list, or -1 if this list does not contain the element.
	 * More formally, returns the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no such index.
	 * @param searched The element to search for
	 * @param values The array that might contain the element.
	 * @return The index of the first occurrence of the specified element inthis list, or -1 if this list does not contain the element.
	 */
	public static <T> int indexOf(T searched, T[] values) {
		for(int x = 0; x < values.length; x++)
			if(values[x].equals(searched))
				return x;
		return -1;
	}
	
	/**
	 * @param x The X Coordinate of the point.
	 * @param y The Y coordinate of the point.
	 * @param px The x coordinate of the point from where the rotation is done.
	 * @param py The y coordinate of the point from where the rotation is done.
	 * @param angle The angle of the rotation in radians.
	 * @return The coordinates of the point after being rotated around a control point by an angle.
	 */
	public static PVector rotatePoint(float x, float y, float px, float py, float angle){
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		// translate point back to origin:
		x -= px;
		y -= py;

		// rotate point
		float xnew = x * c - y * s;
		float ynew = x * s + y * c;

		// translate point back:
		x = xnew + px;
		y = ynew + py;
		return new PVector(x, y);
	}
	
	/**
	 * @param centerX The X coordinate of the rectangle's center.
	 * @param centerY The Y coordinate of the rectangle's center.
	 * @param width The rectangle's width.
	 * @param height The rectangle's height.
	 * @param angle The angle in radians by which the rectangle is rotated.
	 * @return The coordinates of the points of the rectangle ([[x1, x2, x3, x4],[y1, y2, y3, y4]])
	 */
	public static int[][] rotateRectangle(int centerX, int centerY, int width, int height, float angle){
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);
		
		float pX = width/2f;
		float pY = height/2f;
		
		return new int[][]{
			{
				(int) (centerX + pX * c + pY * s),
				(int) (centerX - pX * c + pY * s),
				(int) (centerX - pX * c - pY * s),
				(int) (centerX + pX * c - pY * s)
			},
			{
				(int) (centerY - pX * s + pY * c),
				(int) (centerY + pX * s + pY * c),
				(int) (centerY + pX * s - pY * c),
				(int) (centerY - pX * s - pY * c)
			}
		};
	}
}
