package com.n1ark.whereitends;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Random;

import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.Window;
import com.n1ark.whereitends.graphics.WorldBuilder.Tool;
import com.n1ark.whereitends.graphics.WorldSelectionMenu;
import com.n1ark.whereitends.graphics.WorldView;
import com.n1ark.whereitends.graphics.component.Button;
import com.n1ark.whereitends.graphics.component.ButtonList;
import com.n1ark.whereitends.graphics.component.IconButton;
import com.n1ark.whereitends.graphics.component.Switch;
import com.n1ark.whereitends.graphics.component.TextInput;
import com.n1ark.whereitends.world.Block;
import com.n1ark.whereitends.world.World;

public class WhereItEnds {
	
	/*
	 * A Random instance.
	 */
	public static final Random random = new Random();
	
	public static void main(String[] args) throws IOException, URISyntaxException, FontFormatException {
		long chrono = System.currentTimeMillis();
		
		ClassLoader cl = WhereItEnds.class.getClassLoader();

		System.out.println("Loading font...");
		try (InputStream fontInput = cl.getResourceAsStream("assets/gui/normal.ttf")) {
			NORMAL_FONT = Font.createFont(Font.TRUETYPE_FONT, fontInput);
			fontInput.close();
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(NORMAL_FONT);
			System.out.println("Done loading font. (" + (System.currentTimeMillis() - chrono) + "ms)");
		} catch (FileNotFoundException e) {
			System.out.println("File not found for fonts");
		}
		
		System.out.println("Loading textures...");
		chrono = System.currentTimeMillis();
		for (Block b : Block.values())
			b.load(cl);
		Player.load(cl);
		Button.load(cl);
		TextInput.load(cl);
		ButtonList.load(cl);
		Switch.load(cl);
		IconButton.load(cl);
		WorldSelectionMenu.load(cl);
		WorldView.load(cl);
		Tool.load(cl);
		System.out.println("Done loading textures. (" + (System.currentTimeMillis() - chrono) + "ms)");
		
		INSTANCE = new WhereItEnds();
		INSTANCE.WINDOW = new Window();
	}

	WhereItEnds() throws URISyntaxException {
		String s = WhereItEnds
				.class
				.getProtectionDomain()
				.getCodeSource()
				.getLocation()
				.toURI()
				.getPath();
		
		s = s.substring(1).substring(0, s.lastIndexOf('/')) + "saves";
	
		/*
		 * En fait, sous Windows le Path commence a la racine meme de l'ordinateur, alors que 
		 * sous macOS ca commence dans le dossieur de l'utilisateur, aka: Users/Utilisateur/Dossier...
		 * Donc il faut retirer les deux premier noms pour avoir le bon path.
		 */
		if(!System.getProperty("os.name").toLowerCase().contains("win")) {
			String b = "";
			int y = 0;
			for(String x : s.split("/")) {
				if(y > 1)
					b+=x+"/";
				y++;
			}
			s=b;
		}
		
		Path ss = FileSystems.getDefault().getPath(s);
		if(!ss.toFile().exists()) {
			ss.toFile().mkdirs();
			System.out.println("Tried creating save file, did I manage? ");
		} else
			System.out.println("Save file already exists");
		this.SAVE_FOLDER = ss;
	}

	/**
	 * The current game's instance.
	 */
	public static WhereItEnds INSTANCE;
	/**
	 * The game's current player.
	 */
	public Player PLAYER;
	/**
	 * The game's main world.
	 */
	public World WORLD;
	/**
	 * The game's window.
	 */
	public Window WINDOW;
	/**
	 * The path where the world saves will be saved.
	 */
	public Path SAVE_FOLDER;
	/**
	 * The basic font that should be used in menus, dialogs, etc.
	 */
	public static Font NORMAL_FONT;
}
