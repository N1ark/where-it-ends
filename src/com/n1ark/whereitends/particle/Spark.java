package com.n1ark.whereitends.particle;

import java.awt.Color;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.world.World;

public class Spark extends Particle {

	/**
	 * The Color of a Spark.
	 */
	protected static final Color COLOR_SPARK = new Color(255, 255, 230);
	/**
	 * By how much should the life be multiplied to get the size.
	 */
	protected final float mult;
	
	public Spark(World w, float x, float y, float direction) {
		super(w, x, y);
		this.vel.x = (float) Math.cos(direction)*0.06f;
		this.vel.y = (float) Math.sin(direction)*0.06f;
		this.life = 30 + WhereItEnds.random.nextInt(20);
		this.mult = (WhereItEnds.random.nextFloat()*0.07f + 0.1f)/this.life;
	}
	
	@Override
	public void tick() {
		super.tick();
		this.width = this.life * this.mult;
	}
	
	@Override
	public float getHeight() {
		return getWidth();
	}

	@Override
	public Color getColor() {
		return COLOR_SPARK;
	}
	
}
