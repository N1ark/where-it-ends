package com.n1ark.whereitends.world;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.n1ark.whereitends.util.OpenSimplexNoise;

public class Chunk {
	/**
	 * The file extension for chunk files.
	 */
	public static final String CHUNK_EXTENSION = "chunk";
	/**
	 * The size of a chunk, on the X axis.
	 */
	public static final int X_SIZE = 128;
	/**
	 * The size of a chunk, on the Y axis.
	 */
	public static final int Y_SIZE = 256;
	/**
	 * The array containing all the blocks of the chunk, can be used by doing blocks[x][y]
	 */
	private Block[][] blocks;
	/**
	 * The index position of the chunk in the world.
	 */
	private int x;
	
	/**
	 * A chunk, representing a section of the world, that spread 128 blocks on the X axis, and 256 blocks in the Y axis.
	 */
	public Chunk(int index) {
		this.x = index;
		this.blocks = new Block[X_SIZE][Y_SIZE];
	}
	
	/**
	 * @return The X index of this chunk in the world where it is.
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * Will generate the Chunk, following the appropriate rules.
	 * @param mode The GenerationMode, that will decide on how will the Chunk be generated.
	 * @param seed The seed of the
	 */
	public void generate(GenerationMode mode, long seed) {
		Random r = new Random(seed + this.x);
		switch(mode) {
		case CLASSIC:
			OpenSimplexNoise land = new OpenSimplexNoise(seed);
			for(int xBlock = 0; xBlock < X_SIZE; xBlock++) {
				int grassY = 40+(int) Math.floor(land.eval((X_SIZE * this.x + xBlock)/20d, 0) * 6d);
				for(int y = 0; y < grassY-18; y++)
					this.blocks[xBlock][y] = Block.STONE;
				for(int y = grassY-18; y < grassY-16; y++)
					this.blocks[xBlock][y] = r.nextBoolean() ? Block.STONE : Block.ROCKS;
				for(int y = grassY-16; y < grassY-3; y++)
					this.blocks[xBlock][y] = Block.ROCKS;
				for(int y = grassY-3; y < grassY; y++)
					this.blocks[xBlock][y] = Block.DIRT;
				this.blocks[xBlock][grassY] = Block.GRASS;
				if(r.nextFloat() < 0.3)
					this.blocks[xBlock][grassY+1] = Block.GRASS_PLANT;
				else
					this.blocks[xBlock][grassY+1] = Block.AIR;
				for(int y = grassY + 2; y < Y_SIZE; y++)
					this.blocks[xBlock][y] = Block.AIR;
				
				if(r.nextFloat() < 0.1)
					this.blocks[xBlock][grassY + 5] = Block.WARN;
			}
			break;
		case EMPTY:
			for(int xBlock = 0; xBlock < X_SIZE; xBlock++)
				for(int y = 0; y < Y_SIZE; y++)
					this.blocks[xBlock][y] = Block.AIR;
			break;
		}
	}
	
	/**
	 * Used to get the block at a certain location, in the chunk.
	 * @param x The X coordinate in the chunk.
	 * @param y The Y coordinate int the chunk.
	 * @return The block at the specified location.
	 */
	public Block getBlock(@SuppressWarnings("hiding") int x, int y) {
		return this.blocks[x][y];
	}
	
	/**
	 * Will set a block at a specific location in the chunk.
	 * @param b The block to be set.
	 * @param x The X coordinate in the chunk.
	 * @param y The Y coordinate in the chunk. 
	 */
	public void setBlock(Block b, int x, int y) {
		this.blocks[x][y] = b;
	}
	
	/**
	 * Saves the chunk to the specified file, loading inside of it all the block data. Its name
	 * will be "<code>INDEX.chunk</code>", and it will be saved as a UTF-8 file.
	 * @param f The path, where the file will be saved.
	 * @throws IOException
	 */
	public void saveTo(Path root) throws IOException {
		List<String> data = new ArrayList<>();
		for(int xBlock = 0; xBlock < X_SIZE; xBlock++) {
			String line = "";
			for(int y = 0; y < Y_SIZE; y++) {
				line += this.blocks[xBlock][y].getID();
			}
			data.add(line);
		}
		Path file = root.resolve(this.x + "." + CHUNK_EXTENSION);
		Files.write(file, data, Charset.forName("UTF-8"));
	}
	
	/**
	 * Loads a chunk, from the specified file, and by fetching all the data it needs from it.
	 * @param w The world in which the file will be. This is used for the seed.
	 * @param index The index of the chunk.
	 * @param f The path, to get the <code>.chunk</code> file.
	 * @return The chunk that was loaded.
	 * @throws IOException
	 */
	public static Chunk loadFrom(World w, int index, Path file) throws IOException {
		List<String> data = Files.readAllLines(file);
		Chunk c = new Chunk(index);
		for(int i = 0; i < X_SIZE; i++) {
			String line = data.get(i);
			for(int j = 0; j < Y_SIZE; j++) {				
				Block b = Block.getByID(line.charAt(j));
				if(b == null) {
					c.generate(w.getGenerationMode(), w.getSeed());
					System.out.println("Found error while loading, generated chunk " + index + ".");
					return c;
				}
				c.blocks[i][j] = b;
			}
		}
		System.out.println("Loaded chunk " + index);
		return c;
	}
}
