package com.n1ark.whereitends.graphics.component;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public abstract class UIComponent implements MouseListener, MouseMotionListener, ComponentListener {
	/**
	 * The X ratio to which the component's center should be placed.
	 * <br> 0.5 should be the middle, 0 the far left and 1 the far right.
	 */
	protected float x;
	/**
	 * The Y ratio to which the component's center should be placed.
	 * <br> 0.5 should be the middle, 0 the top and 1 the bottom.
	 */
	protected float y;
	/**
	 * The X position of the component on the screen. This is an ever-changing value.
	 */
	protected int xPosition;
	/**
	 * The Y position of the component on the screen. This is an ever changing value.
	 */
	protected int yPosition;
	/**
	 * The button's current width.
	 */
	protected int width;
	/**
	 * The button's current height.
	 */
	protected int height;
	/**
	 * If the button needs to listen to input, such as mouse movement and clicks, and then call the Listeners.
	 * <br>A UIComponent that isn't used shouldn't be listening.
	 */
	protected boolean listening;
	/**
	 * The component in which this UIComponent belongs.
	 */
	protected Component mother;
	/**
	 * If the component was already recalculated.
	 */
	private boolean loaded;
	
	UIComponent(Component mother, float x, float y){
		this.x = x;
		this.y = y;
		this.mother = mother;
		this.listening = false;
		this.loaded = false;
		mother.addMouseListener(this);
		mother.addMouseMotionListener(this);
		mother.addComponentListener(this);
	}
	
	/**
	 * @return The X ratio to which the component's center should be placed.
	 * <br> 0.5 should be the middle, 0 the far left and 1 the far right.
	 */
	public float getX() {
		return this.x;
	}
	
	/**
	 * @return The Y ratio to which the component's center should be placed.
	 * <br> 0.5 should be the middle, 0 the top and 1 the bottom.
	 */
	public float getY() {
		return this.y;
	}
	
	/**
	 * @return The component in which this UIComponent belongs. This will be used to access mouse data,
	 * keyboard data, etc.
	 */
	public Component getMother() {
		return this.mother;
	}

	/**
	 * @return The width of this component.
	 */
	public int getWidth() {
		return this.width;
	}
	
	/**
	 * @return The height of this component.
	 */
	public int getHeight() {
		return this.height;
	}
	
	/**
	 * Will set wether or not the Component is listening to input. If set to false, it 
	 * @param isListening If it needs to send status updates and listen to events.
	 */
	public void setListening(boolean isListening) {
		if(isListening && !this.listening && this.loaded)
			recalculate();
		this.listening = isListening;
	}
	
	/**
	 * @return If the Component is listening to events, and is sending information. By default, it is set to false.
	 */
	public boolean isListening() {
		return this.listening;
	}
	
	/**
	 * The paint method of this component, that will display itself on top of it's mother.
	 * @param g The graphics to paint over.
	 */
	public abstract void paint(Graphics g);
	
	/**
	 * Should simply recalculate everything needed for the component: the font sizes, the positions, etc.
	 */
	protected abstract void recalculate();

	@Override
	public void componentResized(ComponentEvent e) {
		if(this.listening || !this.loaded) {
			recalculate();
			this.loaded = true;
		}
	}
}
