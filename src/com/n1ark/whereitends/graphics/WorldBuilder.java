package com.n1ark.whereitends.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.component.Button;
import com.n1ark.whereitends.graphics.component.Button.ButtonListener;
import com.n1ark.whereitends.graphics.component.UIComponent;
import com.n1ark.whereitends.util.IVector;
import com.n1ark.whereitends.util.Keyboard;
import com.n1ark.whereitends.util.PVector;
import com.n1ark.whereitends.util.Util;
import com.n1ark.whereitends.world.Block;
import com.n1ark.whereitends.world.Chunk;
import com.n1ark.whereitends.world.World;

public class WorldBuilder extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener, ComponentListener, ButtonListener, KeyListener {
	private static final long serialVersionUID = -354529647416221525L;

	/**
	 * The world that will be rendered by this WorldView.
	 */
	private final World world;
	/**
	 * The amount of blocks that will be rendered on the length of the window.
	 */
	private int blocksX;
	/**
	 * The amount of blocks that will be rendered on the height of the window.
	 */
	private int blocksY;
	/**
	 * The size, in pixels, of a block.
	 */
	private int sizeBlock;
	/**
	 * Half of the current width of the WorldView.
	 */
	private int widthH;
	/**
	 * Half of the current height of the WorldView.
	 */
	private int heightH;
	/**
	 * The reference for the center of the screen.
	 */
	private PVector center;
	/**
	 * Where the center of the screen should go.
	 */
	private PVector goal;
	/**
	 * The window in which this WorldBuilder is located.
	 */
	private Window mother;
	/**
	 * The width of the toolbar, that should fill the entire height of the screen
	 */
	private int toolbarW = 250;
	/**
	 * The selected block, to paint the terrain.
	 */
	private Block selected = Block.GRASS;
	/**
	 * The speed at which the camera will move. This value must be superior or equals to 0; 0 would mean it follows instantly the player,
	 * and the higher it is, the slower it moves.
	 */
	public int FOLLOW_SPEED;
	/**
	 * The currently held button. This can be used anywhere, but mainly in the MouseDragged event,
	 * where no information on the button is available.
	 */
	private int heldButton;
	/**
	 * The size of a block in the toolbar.
	 */
	private int size;
	/**
	 * The gap between two blocks in the toolbar.
	 */
	private int hole; 
	/**
	 * The size of the color picker (the saturation-brightness one)
	 */
	private int cpSize;
	/**
	 * The currently selected Tool, for editing the terrain.
	 */
	private Tool tool;
	/**
	 * The position of the mouse.
	 */
	private Point mousePosition;
	/**
	 * If the WorldView is currently paused. If it is true, the background will still render, but a gray overlay and button will appear on top.
	 */
	private boolean paused;
	/**
	 * The color that will fill the screen when the game is paused.
	 */
	private static final Color PAUSE_COLOR = new Color(50, 50, 50, 130);
	/**
	 * The color that will fill the currently selected block.
	 */
	private static final Color HOVER_COLOR = new Color(220, 220, 220, 100);
	/**
	 * The color that will fill the block at the world's spawn location
	 */
	private static final Color SPAWN_COLOR = new Color(40, 40, 255, 100);
	/**
	 * The button to allow the user to resume playing. This is only displayed when the screen in paused.
	 */
	private final Button resume;
	/**
	 * The button to allow the player to go back to the main menu. This is only displayed when the screen is paused.
	 */
	private final Button mainMenu;
	/**
	 * The image for the color picker. It is used to reduce draw time.
	 */
	private final Image colorPicker;
	/**
	 * The image for the hue picker, only calculated once.
	 */
	private final Image huePicker;
	/**
	 * The total amount of frames drew.
	 */
	private int frames = 0;
	/**
	 * The font used to display stuff.
	 */
	private static final Font smallFont = WhereItEnds.NORMAL_FONT.deriveFont(Font.PLAIN, 16);
	/**
	 * The size of the pen tool.
	 */
	private int penSize;

	/**
	 * Created a WorldBuilder panel, that will show the position of the player in the given world.
	 * @param w The world in which the builder should be located. This panel won't check that the player is in this
	 * world, so using a world that isn't the player's should be avoided.
	 * @param window 
	 */
	public WorldBuilder(World w, Window window) {
		this.world = w;
		this.mother = window;
		this.blocksX = 30;
		this.widthH = getWidth()/2;
		this.heightH = getHeight()/2;
		this.FOLLOW_SPEED = 5;
		this.size = this.toolbarW/5;
		this.hole = this.size / 5;
		this.cpSize = 3*this.size+2*this.hole;
		this.tool = Tool.PENCIL;
		this.penSize = 1;
		this.center = w.getSpawnLocation();
		this.goal = this.center.get();
		this.resume = new Button("Resume", this, 0.5f, 0.4f);
		this.mainMenu = new Button("Main Menu", this, 0.5f, 0.7f);
		
		this.resume.addButtonListener(this);
		this.mainMenu.addButtonListener(this);
		
		addMouseWheelListener(this);
		addMouseListener(this);
		addComponentListener(this);
		addMouseMotionListener(this);
		this.mother.addKeyListener(this);
		setFont(smallFont);
		
		this.colorPicker = new BufferedImage(this.cpSize, this.cpSize, BufferedImage.TYPE_INT_RGB);
		this.huePicker = new BufferedImage(this.size, this.cpSize, BufferedImage.TYPE_INT_RGB);

		int[] pixels = ((DataBufferInt) ((BufferedImage) this.huePicker).getRaster().getDataBuffer()).getData();
		int color;
		for(int y = 0; y < this.cpSize; y++) {
			color = Color.getHSBColor((float)y/this.cpSize, 1, 1).getRGB();
			for(int x = 0; x < this.size; x++)
				pixels[y*this.size+x] = color;
		}
		
		redrawColorPicker();
	}
	
	/**
	 * @return If the WorldView is currently paused. If it is true, the background will still render, but a gray overlay and button will appear on top.
	 */
	public boolean isPaused() {
		return this.paused;
	}
	
	/**
	 * Will set the pause state of the screen.
	 * @param paused If it is paused.
	 */
	public void setPaused(boolean isPaused) {
		this.paused = isPaused;
		this.mainMenu.setListening(this.paused);
		this.resume.setListening(this.paused);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		boolean showBounds = Keyboard.isPressed(KeyEvent.VK_Q);
		
		// Sky
		g.setColor(this.world.getColor());
		g.fillRect(0, 0, getWidth()-this.toolbarW, getHeight());
		if(this.center.y - this.blocksY/2 < 0) {
			g.setColor(this.world.getColor().darker());
			g.fillRect(0, y(-1), getWidth()-this.toolbarW, getHeight());
		}
		
		// Blocks
		g.setColor(Color.BLACK);
		for (int bx = (int) this.center.x - this.blocksX / 2; bx < this.center.x + this.blocksX / 2; bx++) {
			int rendX = x(bx);
		for (int by = (int) this.center.y - this.blocksY / 2; by < this.center.y + this.blocksY / 2; by++) {
			int rendY = y(by);
				Block b = this.world.get(bx, by);
				if(b.hasTexture())
					g.drawImage(b.getImage(this.frames), rendX, rendY, this.sizeBlock, this.sizeBlock, this);
				if(showBounds && this.sizeBlock >= 40) {
					g.drawString("x " + bx, rendX+3, rendY+10);
					g.drawString("y " + by, rendX+3, rendY+20);
				}
			}
		}
		
		PVector spawn = this.world.getSpawnLocation();
		if(
				spawn.x > this.center.x - this.blocksX/2 &&
				spawn.x < this.center.x + this.blocksX/2 &&
				spawn.y > this.center.y - this.blocksY/2 &&
				spawn.y < this.center.y + this.blocksY/2
		) {
			int sx = x(spawn.x)-this.sizeBlock/2;
			int sy = y(spawn.y)-this.sizeBlock/2;
			g.setColor(SPAWN_COLOR);
			g.fillRect(sx, sy, this.sizeBlock, this.sizeBlock);
			g.drawImage(Player.FACES[0], sx, sy, this.sizeBlock, this.sizeBlock, this);
		}
		
		if(this.mousePosition != null) {
			g.setColor(Color.WHITE);
			g.drawRect(x((float)Math.floor(revX(this.mousePosition.x))), y((float) Math.floor(revY(this.mousePosition.y))), this.sizeBlock, this.sizeBlock);
			g.setColor(HOVER_COLOR);
			g.fillRect(x((float)Math.floor(revX(this.mousePosition.x))), y((float) Math.floor(revY(this.mousePosition.y))), this.sizeBlock, this.sizeBlock);
		}
		
		
		// Debugging
		if(showBounds) {
			if(this.sizeBlock > 15) {
				for (int bx = (int) this.center.x - this.blocksX / 2; bx < this.center.x + this.blocksX / 2; bx++) {
					int rendX = x(bx);
					if(bx % Chunk.X_SIZE == 0) {
						g.setColor(Color.RED);
						g.fillRect(rendX-1, 0, 3, getHeight());
						g.setColor(Color.BLACK);
					} else
						g.drawLine(rendX, 0, rendX, getHeight());
				}
				for (int by = (int) this.center.y - this.blocksY / 2; by < this.center.y + this.blocksY / 2; by++) {
					int rendY = y(by);
					g.drawLine(0, rendY, getWidth(), rendY);
				}
			} else if(this.sizeBlock > 4){
				for (int bx = (int) this.center.x - this.blocksX / 2; bx < this.center.x + this.blocksX / 2; bx++) {
					if(bx % Chunk.X_SIZE == 0) {
						int rendX = x(bx);
						g.setColor(Color.RED);
						g.fillRect(rendX-1, 0, 3, getHeight());
						g.setColor(Color.BLACK);
					}
				}
			}
		}
		
		if(Keyboard.isEnabled(KeyEvent.VK_F3)) {
			String[] data = {
					"Position: " + this.center.x,
					"                " + this.center.y,
					"FPS: " + this.mother.FRAMES			
				};
			g.setColor(Color.BLACK);
			int yTxt = 10;
			for(String line : data) {
				g.drawString(line, 10, yTxt);
				yTxt += 15;
			}		
		}
		
		// Toolbar
		g.setColor(Color.GRAY);
		g.fillRect(getWidth() - this.toolbarW, 0, this.toolbarW, getHeight());
		Point p = getMousePosition();
		int x = getWidth() - this.toolbarW + this.hole;
		int y = this.hole;
		g.setColor(Color.LIGHT_GRAY);
		
		// Blocks
		for(Block b : Block.values()) {
			if(!b.hasTexture() && b != Block.AIR)
				continue;
			g.setColor(b == this.selected ? new Color(0xFFA0FFA0) : Color.LIGHT_GRAY);
			g.fillRect(x - this.hole/3, y - this.hole/3, this.size + 2*this.hole/3, this.size + 2*this.hole/3);
			g.drawImage(b.getImage(this.frames), x, y, this.size, this.size, this);
			if(p != null && p.x > x && p.x < x+this.size && p.y > y && p.y < y+this.size) {
				g.setColor(Color.BLACK);
				g.drawString(b.getFilename()[0], p.x-4, p.y-8);
			}
			x += this.size + this.hole;
			if(x >= getWidth() - this.hole) {
				y += this.size + this.hole;
				x = getWidth() - this.toolbarW + this.hole;
			}
		}
		
		// Tools
		x = getWidth() - this.toolbarW + this.hole;
		y = getHeight() - this.cpSize - 2*this.hole - this.size;
		for(Tool t : Tool.values()) {
			g.setColor(this.tool == t ? new Color(0xFFE0E0E0) : Color.LIGHT_GRAY);
			g.fillRect(x-this.hole/3, y-this.hole/3, this.size+2*this.hole/3, this.size+2*this.hole/3);
			g.drawImage(t.texture, x, y, this.size, this.size, this);
			if(t == Tool.BRUSH) {
				g.setColor(Color.BLACK);
				g.drawString(String.valueOf(this.penSize), (int) (x+this.size*0.65), (int) (y+this.size*0.9));
			}
			x += this.size + this.hole;
			if(x >= getWidth() - this.hole) {
				y += this.size + this.hole;
				x = getWidth() - this.toolbarW + this.hole;
			}
		}
		
		// Color picker
		x = getWidth() - this.toolbarW + this.hole;
		y = getHeight() - this.cpSize - this.hole;
	
		float[] hsb = Color.RGBtoHSB(
				this.world.getColor().getRed(), 
				this.world.getColor().getGreen(), 
				this.world.getColor().getBlue(), 
				null
		);
		
		g.drawImage(this.colorPicker, x, y, this.cpSize, this.cpSize, this);
		g.drawImage(this.huePicker, x+this.cpSize+this.hole, y, this.size, this.cpSize, this);
		
		g.setColor(Color.BLACK);
		g.fillRect(x + this.cpSize + this.hole, (int) (y+hsb[0]*this.cpSize), this.size, 1);
		g.fillOval(x + (int) (hsb[1]*this.cpSize)-2, y + (int) (hsb[2]*this.cpSize) -2, 5, 5);
		
		if(this.paused) {
			g.setColor(PAUSE_COLOR);
			g.fillRect(0, 0, getWidth(), getHeight());
			this.mainMenu.paint(g);
			this.resume.paint(g);
		} else {
			this.frames++;
			this.center.x = (this.center.x * this.FOLLOW_SPEED + this.goal.x) / (this.FOLLOW_SPEED+1);
			this.center.y = (this.center.y * this.FOLLOW_SPEED + this.goal.y) / (this.FOLLOW_SPEED+1);
		}
		
		this.mother.CURRENT_FRAMES++;
	}
	
	/**
	 * @param where The X coordinate that is will be placed.
	 * @return The X pixel position where a given X coordinate is located, when considering that the first float
	 * is the X coordinate of the middle of the screen.
	 */
	private int x(float where) {
		return this.widthH + (int)Math.floor((where - this.center.x) * this.sizeBlock);
	}
	
	/**
	 * @param where The x point on the screen.
	 * @return Will return the x position on the map, compared to the current view of the map. 
	 * <br>Normally, <code>revX(x(value)) = value</code>
	 */
	private float revX(int where) {
		return this.center.x + (float) (where-this.widthH) / this.sizeBlock;
	}
	
	/**
	 * @param where The Y coordinate that is will be placed.
	 * @return The Y pixel position where a given Y coordinate is located, when considering that the first float
	 * is the Y coordinate of the middle of the screen.
	 */
	private int y(float where) {
		return this.heightH - (int) Math.floor((where - this.center.y) * this.sizeBlock);
	}
	
	/**
	 * @param where The y point on the screen.
	 * @return Will return the y position on the map, compared to the current view of the map. 
	 * <br>Normally, <code>revY(y(value)) = value</code>
	 */
	float revY(int where) {
		return this.center.y - (float) (where-this.heightH) / this.sizeBlock + 1;
	}
	
	/**
	 * Will redraw the color picker.
	 */
	private void redrawColorPicker() {
		int[] pixels = ((DataBufferInt) ((BufferedImage) this.colorPicker).getRaster().getDataBuffer()).getData();
		int color;
		float[] hsb = Color.RGBtoHSB(
				this.world.getColor().getRed(), 
				this.world.getColor().getGreen(), 
				this.world.getColor().getBlue(), 
				null
		);

		for(int x = 0; x < this.cpSize; x++) {
			for(int y = 0; y < this.cpSize; y++) {
				color = Color.getHSBColor(hsb[0], (float)x/this.cpSize, (float)y/this.cpSize).getRGB();
				pixels[y*this.cpSize+x] = color;
			}
		}
	}
	
	/**
	 * Will do what is needed, when a part of the color picker is clicked (changing the hue, updating the color picker image, etc.)
	 * @param x The x coordinate of the mouse when it clicked.
	 * @param y The y coordinate of the mouse when it clicked.
	 */
	private void interactColorPicker(int x, int y) {
		// Saturation/Brightness select
		int xa = getWidth() - this.toolbarW + this.hole;
		int ya = getHeight() - this.cpSize - this.hole;
		if(
				x > xa && 
				x < xa + this.cpSize &&
				y > ya &&
				y < ya + this.cpSize
		) {
			float[] hsb = Color.RGBtoHSB(
					this.world.getColor().getRed(), 
					this.world.getColor().getGreen(), 
					this.world.getColor().getBlue(), 
					null
			);
			float sat = (float) (x - xa)/this.cpSize;
			float bri = (float) (y - ya)/this.cpSize;
			this.world.setColor(Color.getHSBColor(hsb[0], sat, bri));
		} 
		
		// Hue select
		else if (
				x > xa + this.cpSize + this.hole && 
				x < xa + this.cpSize + this.hole + this.size &&
				y > ya &&
				y < ya + this.cpSize
		) {
			float[] hsb = Color.RGBtoHSB(
					this.world.getColor().getRed(), 
					this.world.getColor().getGreen(), 
					this.world.getColor().getBlue(), 
					null
			);
			float hue = (float) (y - ya)/this.cpSize;
			this.world.setColor(Color.getHSBColor(hue, hsb[1], hsb[2]));
			redrawColorPicker();
		}
	}
	

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
		if(!this.paused && e.getX() < getWidth() - this.toolbarW) {
			this.blocksX += e.getWheelRotation() * 3;
			if (this.blocksX < 10)
				this.blocksX = 10;
			if (this.blocksX > 300)
				this.blocksX = 300;
			this.sizeBlock = (int) Math.ceil((double) (getWidth()-this.toolbarW) / (this.blocksX-4));
			this.blocksY = (int) Math.ceil((double) getHeight() / this.sizeBlock) + 4;
		}
	}
	
	@Override
	public void componentResized(ComponentEvent evt) {
		this.widthH = (getWidth()-this.toolbarW)/2;
		this.heightH = getHeight()/2;
		this.sizeBlock = (int) Math.ceil((double) (getWidth()-this.toolbarW) / (this.blocksX-4));
		this.blocksY = (int) Math.ceil((double) getHeight() / this.sizeBlock) + 4;
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(!this.paused) {
			if(e.getX() > getWidth() - this.toolbarW && this.heldButton == MouseEvent.BUTTON1) {
				int x; 
				int y;
							
				interactColorPicker(e.getX(), e.getY());
				
				// Tool select
				x = getWidth() - this.toolbarW + this.hole;
				y = getHeight() - this.cpSize - 2*this.hole - this.size;
				for(Tool t : Tool.values()) {
					if(
							e.getX() > x - this.hole/3 &&
							e.getX() < x+this.size+this.hole/3 &&
							e.getY() > y-this.hole/3 &&
							e.getY() < y+this.size+this.hole/3
					) {
						this.tool = t;
					}
					x += this.size + this.hole;
					if(x >= getWidth() - this.hole) {
						y += this.size + this.hole;
						x = getWidth() - this.toolbarW + this.hole;
					}
				}
				
				// Block select
				y = this.hole;
				x = getWidth() - this.toolbarW + this.hole;
				for(Block b : Block.values()) {
					if(!b.hasTexture() && b != Block.AIR)
						continue;
					if(
							e.getX() > x-this.hole/3 && 
							e.getX() < x+this.size+this.hole/3 &&
							e.getY() > y-this.hole/3 &&
							e.getY() < y+this.size+this.hole/3
					) {
						this.selected = b;
						return;
					}
					x += this.size + this.hole;
					if(x >= getWidth() - this.hole) {
						y += this.size + this.hole;
						x = getWidth() - this.toolbarW + this.hole;
					}
				}
				
				
			} else if (this.heldButton == MouseEvent.BUTTON1){
				switch(this.tool) {
				case PENCIL:
					this.world.set(this.selected, (int)Math.floor(revX(e.getX())), (int) revY(e.getY()));
					break;
				case BRUSH:
					int px = (int) revX(e.getX());
					int py = (int) revY(e.getY());
					for(int x = px - this.penSize; x <= px + this.penSize; x++) {
						for(int y = py - this.penSize; y <= py + this.penSize; y++) {
							if(y > -1 && y < 256 && Math.sqrt(Math.pow(px-x, 2) + Math.pow(py-y, 2)) <= this.penSize) {
								this.world.set(this.selected, x-1, y);
							}
						}
					}
					break;
				case SPAWN:
					PVector loc = new PVector();
					loc.x = revX(e.getX());
					loc.y = revY(e.getY())-1;
					this.world.setSpawnLocation(loc);
					break;
				case BUCKET:
					new Thread(new Runnable() {
						@Override
						public void run() {
							List<IVector> found = new ArrayList<>();
							List<IVector> looping = new ArrayList<>();
							List<IVector> current = new ArrayList<>();
							int rx = (int) Math.floor(revX(e.getX()));
							int ry = (int) Math.floor(revY(e.getY()));
							IVector start = new IVector(rx, ry);
							Block type = WorldBuilder.this.world.get(rx, ry);
							IVector[] directions = new IVector[] {new IVector(1, 0), new IVector(-1, 0), new IVector(0, 1), new IVector(0, -1)};
							IVector loop = new IVector();
							looping.add(start);
							
							while(!looping.isEmpty()) {
								if(found.size() > 10_000) {
									System.out.println("Stopped looping, too many blocks!");
									found.addAll(looping);
									break;
								}
								for(int i = looping.size()-1; i >= 0; i--) {
									IVector pv = looping.get(i);
									for(IVector dir : directions) {
										IVector.add(pv, dir, loop);
										if(
												loop.y > -1 &&
												loop.y < Chunk.Y_SIZE &&
												WorldBuilder.this.world.get(loop.x, loop.y) == type &&
												!looping.contains(loop) && 
												!current.contains(loop) &&
												!found.contains(loop)
										) {
											current.add(loop.get());
										}
									}
								}
								found.addAll(looping);
								looping.clear();
								looping.addAll(current);
								current.clear();
							}
							for(IVector pv : found)
								WorldBuilder.this.world.set(WorldBuilder.this.selected, pv.x, pv.y);
						}
					}).start();
					break;
				}
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(!this.paused) {
			this.mousePosition = e.getPoint();
			if(this.heldButton == MouseEvent.BUTTON1) {
				if(e.getX() < getWidth() - this.toolbarW) {
					if(this.tool == Tool.PENCIL)
						this.world.set(this.selected, (int) Math.floor(revX(e.getX())), (int) Math.floor(revY(e.getY())));
					if(this.tool == Tool.BRUSH) {
						int px = (int) revX(e.getX());
						int py = (int) revY(e.getY());
						for(int x = px - this.penSize; x <= px + this.penSize; x++) {
							for(int y = py - this.penSize; y <= py + this.penSize; y++) {
								if(y > -1 && y < 256 && Math.sqrt(Math.pow(px-x, 2) + Math.pow(py-y, 2)) <= this.penSize) {
									this.world.set(this.selected, x-1, y);
								}
							}
						}
					}
				}
				else {
					interactColorPicker(e.getX(), e.getY());
				}
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(!this.paused) {
			this.heldButton = e.getButton();
			if(e.getX() < getWidth() - this.toolbarW && this.heldButton == MouseEvent.BUTTON3) {
				this.goal.set(revX(e.getX()), revY(e.getY()));
			}
		}
	}
	
	@Override
	public void buttonPressed(UIComponent b) {
		if(b == this.resume) {
			setPaused(false);
		} else {
			try {
				this.world.saveTo(WhereItEnds.INSTANCE.SAVE_FOLDER);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mother.switchToWorldSelectMenu();
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(!this.paused)
			this.mousePosition = e.getPoint();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return this.mother.getSize();
	}
	
	public static enum Tool {
		PENCIL, BRUSH, BUCKET, SPAWN;
		
		private Image texture;
		
		public static void load(ClassLoader cl) throws IOException {
			for(Tool t : values()) {
				t.texture = ImageIO.read(cl.getResourceAsStream("assets/gui/" + t.toString().toLowerCase() + ".png"));
			}
		}
	}

	@Override
	public void componentHidden(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentMoved(ComponentEvent e) {/*Not used*/}
	@Override
	public void componentShown(ComponentEvent e) {/*Not used*/}
	@Override
	public void mouseClicked(MouseEvent e) {/*Not used*/}
	@Override
	public void mouseEntered(MouseEvent e) {/*Not used*/}
	@Override
	public void mouseExited(MouseEvent e) {/*Not used*/}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP && this.penSize < 30)
			this.penSize++;
		else if (e.getKeyCode() == KeyEvent.VK_DOWN && this.penSize > 1)
			this.penSize--;
		else if (e.getKeyCode() == KeyEvent.VK_LEFT && this.selected != Block.values()[0]) 
			this.selected = Block.values()[Util.indexOf(this.selected, Block.values()) - 1];
		else if (e.getKeyCode() == KeyEvent.VK_RIGHT && this.selected != Block.values()[Block.values().length-1])
			this.selected = Block.values()[Util.indexOf(this.selected, Block.values()) + 1];
	}
	@Override
	public void keyReleased(KeyEvent arg0) {/*Not used*/}
	@Override
	public void keyTyped(KeyEvent arg0) {/*Not used*/}
}
