package com.n1ark.whereitends.graphics.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.n1ark.whereitends.WhereItEnds;

/**
 * @author oscar
 *
 */
public class TextInput extends UIComponent implements KeyListener{
	
	/**
	 * The input field's texture.
	 */
	protected static Image TEXTURE;
	/**
	 * The input field's texture when writing.
	 */
	protected static Image TEXTURE_EDITING;
	
	/**
	 * The String currently in the TextInput.
	 */
	protected String text;
	
	/**
	 * The String that will appear if the field is empty.
	 */
	protected final String description;
	
	/**
	 * If the text box is currently empty or only composed of spaces.
	 */
	protected boolean empty;
	
	/**
	 * If the TextInput is currently selected and receiving input.
	 */
	protected boolean selected;
	
	/**
	 * The font's size.
	 */
	protected float fontSize;
	
	/**
	 * The max amount of characters accepted in the String.
	 */
	protected final short maxChars;
	
	/**
	 * All the TextInputListeners waiting for updates from the TextInput.
	 */
	protected List<TextInputListener> listeners;
	
	public TextInput(String input, Component mother, JFrame keyGetter, float x, float y) {
		this(input, null, (short) 16, mother, keyGetter, x, y);
	}
	
	public TextInput(String input, String description, Component mother, JFrame keyGetter, float x, float y) {
		this(input, description, (short) 16, mother, keyGetter, x, y);
	}
	
	public TextInput(String input, short maxChars, Component mother, JFrame keyGetter, float x, float y) {
		this(input, null, maxChars, mother, keyGetter, x, y);
	}
	
	public TextInput(String input, String description, short maxChars, Component mother, JFrame keyGetter, float x, float y) {
		super(mother, x, y);
		this.text = input;
		this.listeners = new ArrayList<>();
		keyGetter.addKeyListener(this);
		this.maxChars = maxChars;
		this.description = description;
		this.empty = this.text.isEmpty() || this.text.replaceAll(" ", "").isEmpty();
	}
	
	/**
	 * Will load the necessary files for this class to work.
	 * @param cl The class loader to get the files.
	 * @throws IOException
	 */
	public static void load(ClassLoader cl) throws IOException {
		TEXTURE = ImageIO.read(cl.getResourceAsStream("assets/gui/text_input.png"));
		TEXTURE_EDITING = ImageIO.read(cl.getResourceAsStream("assets/gui/text_input_writing.png"));
	}
	
	/**
	 * @return The String that is currently in the TextInput.
	 */
	public String getText() {
		return this.text;
	}
	
	/**
	 * @return If the text in this TextInput is empty, or only composed of spaces.
	 */
	public boolean isEmpty() {
		return this.empty;
	}
	
	public void addTextInputListener(TextInputListener til) {
		this.listeners.add(til);
	}
	
	@Override
	protected void recalculate() {
		this.width = this.mother.getWidth()/4;
		this.height = 9 * this.width / 28;
		this.xPosition = (int) (this.mother.getWidth() * this.x - this.width/2);
		this.yPosition = (int) (this.mother.getHeight() * this.y - this.height/2);
		redoFontSize();
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawImage(this.selected ? TEXTURE_EDITING : TEXTURE, this.xPosition, this.yPosition, this.width, this.height, this.mother);
		

		if(!this.empty) {
			g.setFont(WhereItEnds.NORMAL_FONT.deriveFont(this.fontSize));
			g.setColor(Color.WHITE);
			g.drawString(this.text, this.xPosition + this.width/10, this.yPosition + this.height * 3 / 4);		
		} else if(this.description != null) {
			g.setFont(WhereItEnds.NORMAL_FONT.deriveFont(this.height*0.8f));
			g.setColor(Color.LIGHT_GRAY);
			g.drawString(this.description, this.xPosition + this.width/10, this.yPosition + this.height * 3 / 4);
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		if (this.listening && this.selected) {
			if (e.getKeyChar() == '' && this.text.length() > 0) {
				this.text = this.text.substring(0, this.text.length()-1);
				this.empty = this.text.isEmpty() || this.text.replaceAll(" ", "").isEmpty();
				redoFontSize();
				for(TextInputListener til : this.listeners)
					til.inputChangedValue(this, this.text);
			}
			else if(
				this.text.length() < this.maxChars &&
				String.valueOf(e.getKeyChar()).matches("[a-zA-Z0-9 _-]")
			) {
				this.text += e.getKeyChar();
				this.empty = this.text.isEmpty() || this.text.replaceAll(" ", "").isEmpty();
				redoFontSize();
				for(TextInputListener til : this.listeners)
					til.inputChangedValue(this, this.text);
			}
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if(this.listening && this.selected) {
			int id = e.getKeyCode();
			if(id == KeyEvent.VK_ESCAPE || id == KeyEvent.VK_ENTER) {
				this.selected = false;
				for(TextInputListener til : this.listeners)
					til.inputChangedState(this, false);
			}
		}
	}
	
	/**
	 * Will recalculate the font size, so the text fits the input box.
	 */
	private void redoFontSize() {
		this.fontSize = (int) (this.height * 0.8);
		Font f = WhereItEnds.NORMAL_FONT.deriveFont(this.fontSize);
		while(this.mother.getGraphics().getFontMetrics(f).stringWidth(this.text) >= this.width * 0.8) {
			this.fontSize -= 1;
			f = f.deriveFont(this.fontSize);
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(this.listening) {
			boolean lastState = this.selected;
			this.selected = 
					e.getX() >= this.xPosition &&
					e.getX() < this.xPosition + this.width &&
					e.getY() >= this.yPosition &&
					e.getY() < this.yPosition + this.height;
			if(lastState != this.selected)
				for(TextInputListener til : this.listeners)
					til.inputChangedState(this, this.selected);
		}
	}
	
	public static interface TextInputListener {
		void inputChangedState(TextInput i, boolean selected);
		void inputChangedValue(TextInput i, String text);
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {/* Not used */}
	@Override
	public void mouseClicked(MouseEvent arg0) {/* Not used */}
	@Override
	public void mouseEntered(MouseEvent arg0) {/* Not used */}
	@Override
	public void mouseExited(MouseEvent arg0) {/* Not used */}
	@Override
	public void mouseDragged(MouseEvent arg0) {/* Not used */}
	@Override
	public void mouseMoved(MouseEvent arg0) {/* Not used */}
	@Override
	public void keyPressed(KeyEvent e) {/* Not used */}
	@Override
	public void componentHidden(ComponentEvent arg0) {/* Not used */}
	@Override
	public void componentMoved(ComponentEvent arg0) {/* Not used */}
	@Override
	public void componentShown(ComponentEvent arg0) {/* Not used */}
}
