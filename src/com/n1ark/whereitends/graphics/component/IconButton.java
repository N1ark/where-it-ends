package com.n1ark.whereitends.graphics.component;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.n1ark.whereitends.graphics.component.Button.ButtonListener;

public class IconButton extends UIComponent {
	/**
	 * The button's texture.
	 */
	protected static Image TEXTURE;
	/**
	 * The button's texture when hovering.
	 */
	protected static Image TEXTURE_HOVER;
	/**
	 * If the mouse is currently above the button.
	 */
	protected boolean hovering;
	/**
	 * The button's font's size.
	 */
	protected float fontSize;
	/**
	 * The Icon of the button.
	 */
	protected final Icon icon;
	/**
	 * The list with all the ButtonListeners, that need to receive a click event.
	 */
	protected List<ButtonListener> listeners;
	
	public IconButton(Component mother, Icon icon, float x, float y) {
		super(mother, x, y);
		this.listeners = new ArrayList<>();
		this.icon = icon;
	}
	
	/**
	 * Will load the necessary files for this class to work.
	 * @param cl The class loader to get the files.
	 * @throws IOException
	 */
	public static void load(ClassLoader cl) throws IOException {
		TEXTURE = ImageIO.read(cl.getResourceAsStream("assets/gui/icon_button.png"));
		TEXTURE_HOVER = ImageIO.read(cl.getResourceAsStream("assets/gui/icon_button_hover.png"));
		for(Icon i : Icon.values())
			i.ICON_TEXTURE = ImageIO.read(cl.getResourceAsStream("assets/gui/" + i.toString().toLowerCase() + "_icon.png"));
			
	}
		
	@Override
	public void paint(Graphics g) {
		g.drawImage(this.hovering ? TEXTURE_HOVER : TEXTURE, this.xPosition, this.yPosition, this.width, this.width, this.mother);
		g.drawImage(this.icon.ICON_TEXTURE, this.xPosition, this.yPosition, this.width, this.width, this.mother);
	}
	
	/**
	 * Adds a Listener to this button, that will have its <code>buttonPressed</code> method triggered when this
	 * button is pressed. This is triggered with the mouse's left click release.
	 * @param b The listener to add.
	 */
	public void addButtonListener(ButtonListener b) {
		this.listeners.add(b);
	}
	
	@Override
	protected void recalculate() {
		this.width = 9*this.mother.getWidth()/5/16;
		this.xPosition = (int) (this.mother.getWidth() * this.x - this.width/2);
		this.yPosition = (int) (this.mother.getHeight() * this.y - this.width/2);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(this.listening && this.hovering && e.getButton() == MouseEvent.BUTTON1)
			for(ButtonListener bl : this.listeners)
				bl.buttonPressed(this);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(this.listening)
			this.hovering = 
				e.getX() >= this.xPosition && 
				e.getX() < this.xPosition + this.width && 
				e.getY() >= this.yPosition && 
				e.getY() < this.yPosition + this.width;
	}
	
	public static enum Icon {
		MORE, BACK, IMAGE, FACE, NEXT;
		private Image ICON_TEXTURE;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {/* Not used */}
	@Override
	public void mouseEntered(MouseEvent e) {/* Not used */}
	@Override
	public void mouseExited(MouseEvent e) {/* Not used */}
	@Override
	public void mousePressed(MouseEvent e) {/* Not used */}
	@Override
	public void mouseDragged(MouseEvent e) {/* Not used */}
	@Override
	public void componentHidden(ComponentEvent e) {/* Not used */}
	@Override
	public void componentMoved(ComponentEvent e) {/* Not used */}
	@Override
	public void componentShown(ComponentEvent e) {/* Not used */}

}
