package com.n1ark.whereitends.world;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author oscar
 *
 */
public enum Block {
	
	STONE('s', "stone"),
	GRASS('g', "grass"),
	DIRT('d', "dirt"),
	ROCKS('r', "rocks"),
	STEEL('t', "steel"),
	WARN('w', "warn"),
	GRASS_PLANT('p', "grass_plant", false),
	VINES('v', "vines", false),
	BLADE('b', false, 10, "blade_1", "blade_2"),
	JUMPER('j', false, 3, "jumper1", "jumper1", "jumper1", "jumper2", "jumper3", "jumper4", "jumper5", "jumper6", "jumper7", "jumper8", "jumper9", "jumper10", "jumper11", "jumper12", "jumper13"),
	COIN('c', false, 8, "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin1", "coin2", "coin3", "coin4", "coin5"),
	COIN_TAKEN('c'), //Here, I can just create another block, that is invisible but will have the same name in the file. Since blocks are found by looping in the normal order, a coin will always be loaded when a taken coin is saved!
	JUMP_POTION('P', false, 5, "potion1", "potion2", "potion3", "potion4", "potion5", "potion6", "potion7", "potion8", "potion9", "potion10", "potion11"),
	JUMP_POTION_TAKEN('P'),
	SIZE_POTION('S', false, 5, "potion_size1", "potion_size1", "potion_size2", "potion_size3", "potion_size4", "potion_size5", "potion_size6", "potion_size7", "potion_size8", "potion_size9", "potion_size1"),
	SIZE_POTION_TAKEN('S'),
	WATER('W', "water", false),
	AIR('_')
	;
	
	/**
	 * The ID this block will be loaded and saved with.
	 */
	private final char id;
	/**
	 * The images of the block. It is an Image, of type ARGB.
	 */
	private final Image img[];
	/**
	 * The names of the files from which should be loaded.
	 */
	private final String fileName[];
	/**
	 * If the block should be loaded from a file.
	 */
	private final boolean shouldBeLoaded;
	/**
	 * If the block can be walked through.
	 */
	private final boolean isSolid;
	/**
	 * If the texture is animated.
	 */
	private final boolean animated;
	/**
	 * Only used if the texture is animated. The amount of frames between each texture.
	 */
	private final int cooldown;
	
	/**
	 * Creates a block, with no texture.
	 * @param id The ID for the block.
	 */
	Block(char id){
		this.id = id;
		this.img = new Image[] {null};
		this.fileName = new String[] {""};
		this.shouldBeLoaded = false;
		this.isSolid = false;
		this.animated = false;
		this.cooldown = 0;
	}
	/**
	 * Creates a block, that must be loaded from a file.
	 * @param id The ID of the block.
	 * @param file The name of the file.
	 */
	Block(char id, String file){
		this.id = id;
		this.img = new Image[1];
		this.shouldBeLoaded = true;
		this.fileName = new String[] {file};
		this.isSolid = true;
		this.animated = false;
		this.cooldown = 0;
	}
	/**
	 * Creates a block, that must be loaded from a file.
	 * @param id The ID of the block.
	 * @param file The name of the file.
	 * @param solid If the player can walk through the block.
	 */
	Block(char id, String file, boolean solid){
		this.id = id;
		this.img = new Image[1];
		this.shouldBeLoaded = true;
		this.fileName = new String[] {file};
		this.isSolid = solid;
		this.animated = false;
		this.cooldown = 0;
	}
	/**
	 * Creates a solid block with multiple textures, that must be loaded from a file.
	 * @param id The ID of the block.
	 * @param cooldown The cooldown in frames between changing each image.
	 * @param files The names of the files.
	 */
	Block(char id, int cooldown, String... files){
		this.id = id;
		this.cooldown = cooldown;
		this.img = new Image[files.length];
		this.shouldBeLoaded = true;
		this.fileName = files;
		this.isSolid = true;
		this.animated = true;
	}
	/**
	 * Creates a block with multiple textures that must be loaded from a file.
	 * @param id The ID of the block.
	 * @param solid If the block is solid.
	 * @param cooldown The cooldown in frames between changing each image.
	 * @param files The names of the files.
	 */
	Block(char id, boolean solid, int cooldown, String... files){
		this.id = id;
		this.cooldown = cooldown;
		this.img = new Image[files.length];
		this.shouldBeLoaded = true;
		this.fileName = files;
		this.isSolid = solid;
		this.animated = true;
	}
	
	/**
	 * @return The ID by which this block will be saved and loaded.
	 */
	public char getID() {
		return this.id;
	}
	
	/**
	 * @param frame 
	 * @return The image to display this block.
	 */
	public Image getImage(int frame) {
		return this.animated ? this.img[(frame/this.cooldown)%this.img.length] : this.img[0];
	}
	
	/**
	 * @return If the block can be walked through.
	 */
	public boolean isSolid() {
		return this.isSolid;
	}
	
	/**
	 * @return If this block has a texture. It will actually return wether or not a texture should be loaded. 
	 */
	public boolean hasTexture() {
		return this.shouldBeLoaded;
	}
	
	/**
	 * @return The names of the files from which the block's texture is loaded. If it is a textureless block, this will be an
	 * empty string. This should not be confused with a getName() method.
	 */
	public String[] getFilename() {
		return this.fileName;
	}
	
	/**
	 * Loads the image for the block.
	 * @param loader The loaded to be used.
	 * @throws IOException
	 */
	public void load(ClassLoader loader) throws IOException {
		if(this.shouldBeLoaded)
			for(int i = 0; i < this.fileName.length; i++)
				this.img[i] = ImageIO.read(loader.getResourceAsStream("assets/block/" + this.fileName[i] + ".png"));
	}
	/**
	 * @return The block corresponding to the given ID.
	 */
	public static Block getByID(char id) {
		for(Block b : values())
			if(b.id == id)
				return b;
		return null;
	}
}