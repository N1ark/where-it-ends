package com.n1ark.whereitends.particle;

import java.awt.Color;

import com.n1ark.whereitends.util.PVector;
import com.n1ark.whereitends.world.World;

public abstract class Particle {
	/**
	 * The position of the Particle.
	 */
	protected final PVector pos;
	/**
	 * The velocity of the Particle.
	 */
	protected final PVector vel;
	/**
	 * The World the particle is in.
	 */
	protected final World world;
	/**
	 * The number of frames that can be drawn, before the Particle disappears.
	 */
	protected int life;
	/**
	 * The width of the particle.
	 */
	protected float width;
	/**
	 * The height of the particle.
	 */
	protected float height;
	/**
	 * The particle's rotation in radians.
	 */
	protected float rotation;
	
	protected Particle(World w, float x, float y){
		this.pos = new PVector(x, y);
		this.vel = new PVector();
		this.world = w;
		this.rotation = 0;
	}
	
	/**
	 * @return The width in blocks of the Particle.
	 */
	public float getWidth() {
		return this.width;
	}
	
	/**
	 * @return The height in blocks of the Particle.
	 */
	public float getHeight() {
		return this.height;
	}

	/**
	 * @return The location of the Particle.
	 */
	public PVector getLocation() {
		return this.pos;
	}
	
	/**
	 * @return The rotation of the particle.
	 */
	public float getRotation() {
		return this.rotation;
	}
	
	/**
	 * @return The color of the Particle.
	 */
	public abstract Color getColor();
	
	/**
	 * Will shorten the life left for the Particle.
	 */
	public void tick() {
		this.life--;
		if(this.life < 0) {
			this.world.removeParticle(this);
			return;
		}
		this.pos.add(this.vel);
	}
}
