package com.n1ark.whereitends.graphics;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.component.Button.ButtonListener;
import com.n1ark.whereitends.graphics.component.IconButton;
import com.n1ark.whereitends.graphics.component.IconButton.Icon;
import com.n1ark.whereitends.graphics.component.UIComponent;

public class PlayerFaceSelect extends JPanel implements ButtonListener, ComponentListener {
	private static final long serialVersionUID = -6760070739396843620L;
	
	/**
	 * The Window that contains this view.
	 */
	private final Window mother;
	/**
	 * The IconButton to go back to the world selection page
	 */
	private final IconButton back;
	/**
	 * The IconButton to switch to the next face.
	 */
	private final IconButton nextFace;
	/**
	 * The IconButton to switch to the previous page.
	 */
	private final IconButton prevFace;
	/**
	 * The ration of the background, on Width/Height.
	 */
	private final float backgroundRatio = 54f/36f;
	/**
	 * The X Position to draw the background image.
	 */
	private int backgroundX;
	/**
	 * The Y Position to draw the background image.
	 */
	private int backgroundY;
	/**
	 * The width of the background, for rendering it.
	 */
	private int backgroundWidth;
	/**
	 * The height of the background, for rendering it.
	 */
	private int backgroundHeight;
	
	private float skinSize;
	private boolean skinSizeIncreasing;

	public PlayerFaceSelect(Window mother) {
		super();
		this.mother = mother;
		this.back = new IconButton(this, Icon.BACK, 0.1f, 0.1f);
		this.nextFace = new IconButton(this, Icon.NEXT, 0.7f, 0.5f);
		this.prevFace = new IconButton(this, Icon.BACK, 0.3f, 0.5f);
		
		this.back.setListening(true);
		this.nextFace.setListening(true);
		this.prevFace.setListening(true);
		
		this.back.addButtonListener(this);
		this.nextFace.addButtonListener(this);
		this.prevFace.addButtonListener(this);
		
		addComponentListener(this);
		this.skinSize = 1;
		this.skinSizeIncreasing = true;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(WorldSelectionMenu.MENU_BACKGROUND, this.backgroundX, this.backgroundY, this.backgroundWidth, this.backgroundHeight, this);
		
		int width = this.nextFace.getWidth();
		g.drawImage(Player.SKIN, (int)(getWidth()-width*this.skinSize)/2, (int)(getHeight()-width*this.skinSize)/2, (int)(width*this.skinSize), (int) (width*this.skinSize), this);
		g.drawImage(Player.FACES[this.mother.selectedFace], (getWidth()-width)/2, (getHeight()-width)/2, width, width, this);
		this.nextFace.paint(g);
		this.prevFace.paint(g);
		this.back.paint(g);
		
		if(this.skinSizeIncreasing) {
			this.skinSize += 0.003;
			if(this.skinSize > 1.5)
				this.skinSizeIncreasing = false;
		} else {
			this.skinSize -= 0.003;
			if(this.skinSize <= 1)
				this.skinSizeIncreasing = true;
		}
	}

	@Override
	public void buttonPressed(UIComponent b) {
		if(b == this.back) {
			this.mother.switchToWorldSelectMenu();
		} else if(b == this.nextFace) {
			this.mother.selectedFace = (this.mother.selectedFace+1)%Player.FACES.length;
		} else if(b == this.prevFace) {
			this.mother.selectedFace = (this.mother.selectedFace == 0 ? Player.FACES.length-1 : this.mother.selectedFace-1);
		}
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		if((double) getWidth() / getHeight() > this.backgroundRatio) {
			this.backgroundWidth = getWidth();
			this.backgroundHeight = (int) (this.backgroundWidth / this.backgroundRatio);
			this.backgroundX = 0;
			this.backgroundY = (getHeight() - this.backgroundHeight)/2;
		} else {
			this.backgroundHeight = getHeight();
			this.backgroundWidth = (int) (this.backgroundHeight * this.backgroundRatio);
			this.backgroundY = 0;
			this.backgroundX = (getWidth() - this.backgroundWidth)/2;
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		return this.mother.getSize();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {/* not used */}

	@Override
	public void componentMoved(ComponentEvent arg0) {/* not used */}

	@Override
	public void componentShown(ComponentEvent arg0) {/* not used */}
	
}
