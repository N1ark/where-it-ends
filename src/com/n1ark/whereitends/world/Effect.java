package com.n1ark.whereitends.world;

public enum Effect {
	NONE, DOUBLE_JUMP, SIZE_PLUS;
}
