package com.n1ark.whereitends.util;

public class IVector {
	/** The x component of the vector. */
	public int x;

	/** The y component of the vector. */
	public int y;

	/** Array so that this can be temporarily used in an array context */
	protected float[] array;

	/**
	 * Constructor for an empty vector: x, and y are set to 0.
	 */
	public IVector() {}

	/**
	 * Constructor for a 2D vector.
	 * 
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 */
	public IVector(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Set x and y coordinates.
	 * 
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 */
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Set x, y, and z coordinates from a Vector3D object.
	 * 
	 * @param v the IVector object to be copied
	 */
	public void set(IVector v) {
		this.x = v.x;
		this.y = v.y;
	}

	/**
	 * Set the x, y (and maybe z) coordinates using a float[] array as the source.
	 * 
	 * @param source array to copy from
	 */
	public void set(int[] source) {
		if (source.length >= 2) {
			this.x = source[0];
			this.y = source[1];
		}
	}

	/**
	 * Get a copy of this vector.
	 */
	public IVector get() {
		return new IVector(this.x, this.y);
	}

	public float[] get(float[] target) {
		if (target == null) {
			return new float[] { this.x, this.y };
		}
		if (target.length >= 2) {
			target[0] = this.x;
			target[1] = this.y;
		}
		return target;
	}

	/**
	 * Calculate the magnitude (length) of the vector
	 * 
	 * @return the magnitude of the vector
	 */
	public float mag() {
		return (float) Math.sqrt(this.x * this.x + this.y * this.y);
	}

	/**
	 * Add a vector to this vector
	 * 
	 * @param v the vector to be added
	 */
	public void add(IVector v) {
		this.x += v.x;
		this.y += v.y;
	}

	@SuppressWarnings("hiding")
	public void add(float x, float y) {
		this.x += x;
		this.y += y;
	}

	/**
	 * Add two vectors
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return a new vector that is the sum of v1 and v2
	 */
	static public IVector add(IVector v1, IVector v2) {
		return add(v1, v2, null);
	}

	/**
	 * Add two vectors into a target vector
	 * 
	 * @param v1     a vector
	 * @param v2     another vector
	 * @param target the target vector (if null, a new vector will be created)
	 * @return a new vector that is the sum of v1 and v2
	 */
	static public IVector add(IVector v1, IVector v2, IVector target) {
		if (target == null) {
			target = new IVector(v1.x + v2.x, v1.y + v2.y);
		} else {
			target.set(v1.x + v2.x, v1.y + v2.y);
		}
		return target;
	}

	/**
	 * Subtract a vector from this vector
	 * 
	 * @param v the vector to be subtracted
	 */
	public void sub(IVector v) {
		this.x -= v.x;
		this.y -= v.y;
	}

	@SuppressWarnings("hiding")
	public void sub(float x, float y) {
		this.x -= x;
		this.y -= y;
	}

	/**
	 * Subtract one vector from another
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return a new vector that is v1 - v2
	 */
	static public IVector sub(IVector v1, IVector v2) {
		return sub(v1, v2, null);
	}

	static public IVector sub(IVector v1, IVector v2, IVector target) {
		if (target == null) {
			target = new IVector(v1.x - v2.x, v1.y - v2.y);
		} else {
			target.set(v1.x - v2.x, v1.y - v2.y);
		}
		return target;
	}

	/**
	 * Multiply this vector by a scalar
	 * 
	 * @param n the value to multiply by
	 */
	public void mult(float n) {
		this.x *= n;
		this.y *= n;
	}

	/**
	 * Multiply a vector by a scalar
	 * 
	 * @param v a vector
	 * @param n scalar
	 * @return a new vector that is v1 * n
	 */
	static public IVector mult(IVector v, float n) {
		return mult(v, n, null);
	}

	/**
	 * Multiply a vector by a scalar, and write the result into a target IVector.
	 * 
	 * @param v      a vector
	 * @param n      scalar
	 * @param target IVector to store the result
	 * @return the target vector, now set to v1 * n
	 */
	static public IVector mult(IVector v, float n, IVector target) {
		if (target == null) {
			target = new IVector((int) (v.x * n), (int) (v.y * n));
		} else {
			target.set((int) (v.x * n), (int) (v.y * n));
		}
		return target;
	}

	/**
	 * Multiply each element of one vector by the elements of another vector.
	 * 
	 * @param v the vector to multiply by
	 */
	public void mult(IVector v) {
		this.x *= v.x;
		this.y *= v.y;
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and return the result as a new IVector.
	 */
	static public IVector mult(IVector v1, IVector v2) {
		return mult(v1, v2, null);
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and write the result into a target vector.
	 * 
	 * @param v1     the first vector
	 * @param v2     the second vector
	 * @param target IVector to store the result
	 */
	static public IVector mult(IVector v1, IVector v2, IVector target) {
		if (target == null) {
			target = new IVector(v1.x * v2.x, v1.y * v2.y);
		} else {
			target.set(v1.x * v2.x, v1.y * v2.y);
		}
		return target;
	}

	/**
	 * Divide this vector by a scalar
	 * 
	 * @param n the value to divide by
	 */
	public void div(float n) {
		this.x /= n;
		this.y /= n;
	}

	/**
	 * Divide a vector by a scalar and return the result in a new vector.
	 * 
	 * @param v a vector
	 * @param n scalar
	 * @return a new vector that is v1 / n
	 */
	static public IVector div(IVector v, float n) {
		return div(v, n);
	}

	static public IVector div(IVector v, float n, IVector target) {
		if (target == null) {
			target = new IVector((int) (v.x / n), (int) (v.y / n));
		} else {
			target.set((int) (v.x / n), (int) (v.y / n));
		}
		return target;
	}

	/**
	 * Divide each element of one vector by the elements of another vector.
	 */
	public void div(IVector v) {
		this.x /= v.x;
		this.y /= v.y;
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and return the result as a new IVector.
	 */
	static public IVector div(IVector v1, IVector v2) {
		return div(v1, v2, null);
	}

	/**
	 * Divide each element of one vector by the individual elements of another
	 * vector, and write the result into a target vector.
	 * 
	 * @param v1     the first vector
	 * @param v2     the second vector
	 * @param target IVector to store the result
	 */
	static public IVector div(IVector v1, IVector v2, IVector target) {
		if (target == null) {
			target = new IVector(v1.x / v2.x, v1.y / v2.y);
		} else {
			target.set(v1.x / v2.x, v1.y / v2.y);
		}
		return target;
	}

	/**
	 * Calculate the Euclidean distance between two points (considering a point as a
	 * vector object)
	 * 
	 * @param v another vector
	 * @return the Euclidean distance between
	 */
	public float dist(IVector v) {
		float dx = this.x - v.x;
		float dy = this.y - v.y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Calculate the Euclidean distance between two points (considering a point as a
	 * vector object)
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return the Euclidean distance between v1 and v2
	 */
	static public float dist(IVector v1, IVector v2) {
		float dx = v1.x - v2.x;
		float dy = v1.y - v2.y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	static public IVector fromAngle(double angle) {
		return new IVector((int) Math.sin(angle), (int) Math.cos(angle));
	}

	/**
	 * Calculate the dot product with another vector
	 * 
	 * @return the dot product
	 */
	public float dot(IVector v) {
		return this.x * v.x + this.y * v.y;
	}

	@SuppressWarnings("hiding")
	public float dot(float x, float y) {
		return this.x * x + this.y * y;
	}

	static public float dot(IVector v1, IVector v2) {
		return v1.x * v2.x + v1.y * v2.y;
	}

	/**
	 * Normalize the vector to length 1 (make it a unit vector)
	 */
	public void normalize() {
		float m = mag();
		if (m != 0 && m != 1) {
			div(m);
		}
	}

	/**
	 * Normalize this vector, storing the result in another vector.
	 * 
	 * @param target Set to null to create a new vector
	 * @return a new vector (if target was null), or target
	 */
	public IVector normalize(IVector target) {
		if (target == null) {
			target = new IVector();
		}
		float m = mag();
		if (m > 0) {
			target.set((int) (this.x / m), (int) (this.y / m));
		} else {
			target.set(this.x, this.y);
		}
		return target;
	}

	/**
	 * Limit the magnitude of this vector
	 * 
	 * @param max the maximum length to limit this vector
	 */
	public void limit(float max) {
		if (mag() > max) {
			normalize();
			mult(max);
		}
	}

	/**
	 * Calculate the angle of rotation for this vector (only 2D vectors)
	 * 
	 * @return the angle of rotation
	 */
	public float heading2D() {
		float angle = (float) Math.atan2(-this.y, this.x);
		return -1 * angle;
	}

	/**
	 * Calculate the angle between two vectors, using the dot product
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return the angle between the vectors
	 */
	static public float angleBetween(IVector v1, IVector v2) {
		double dot = v1.x * v2.x + v1.y * v2.y;
		double v1mag = Math.sqrt(v1.x * v1.x + v1.y * v1.y);
		double v2mag = Math.sqrt(v2.x * v2.x + v2.y * v2.y);
		return (float) Math.acos(dot / (v1mag * v2mag));
	}

	@Override
	public String toString() {
		return "[ " + this.x + ", " + this.y + " ]";
	}

	/**
	 * Return a representation of this vector as a float array. This is only for
	 * temporary use. If used in any other fashion, the contents should be copied by
	 * using the get() command to copy into your own array.
	 */
	public float[] array() {
		if (this.array == null) {
			this.array = new float[3];
		}
		this.array[0] = this.x;
		this.array[1] = this.y;
		return this.array;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof IVector && this.x == ((IVector) obj).x && this.y == ((IVector) obj).y;
	}

}
