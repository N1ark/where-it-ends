package com.n1ark.whereitends.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.n1ark.whereitends.WhereItEnds;
import com.n1ark.whereitends.entity.Player;
import com.n1ark.whereitends.graphics.component.Button;
import com.n1ark.whereitends.graphics.component.Button.ButtonListener;
import com.n1ark.whereitends.graphics.component.UIComponent;
import com.n1ark.whereitends.particle.Particle;
import com.n1ark.whereitends.util.Keyboard;
import com.n1ark.whereitends.util.PVector;
import com.n1ark.whereitends.util.Util;
import com.n1ark.whereitends.world.Block;
import com.n1ark.whereitends.world.Chunk;
import com.n1ark.whereitends.world.Effect;
import com.n1ark.whereitends.world.World;

public class WorldView extends JPanel implements MouseWheelListener, ComponentListener, ButtonListener {
	private static final long serialVersionUID = -354529647416221525L;
	
	/**
	 * The back image for the bar for the height/width indicator.
	 */
	public static Image BAR_BACK;
	/**
	 * The front image for the bar for the height/width indicator.
	 */
	public static Image BAR_FRONT;
	/**
	 * The world that will be rendered by this WorldView.
	 */
	private final World world;
	/**
	 * The amount of blocks that will be rendered on the length of the window.
	 */
	private int blocksX;
	/**
	 * The amount of blocks that will be rendered on the height of the window.
	 */
	private int blocksY;
	/**
	 * The size, in pixels, of a block.
	 */
	private int sizeBlock;
	/**
	 * Half of the current width of the WorldView.
	 */
	private int widthH;
	/**
	 * Half of the current height of the WorldView.
	 */
	private int heightH;
	/**
	 * The zoom value of the renderer, that ranges from 0 to see 10 blocks, to 100 to see 100 blocks.
	 */
	private byte zoom;
	/**
	 * The reference for the center of the screen.
	 */
	private PVector center;
	/**
	 * The window in which this WorldView is located.
	 */
	private Window mother;
	/**
	 * The player to be displayed in the world.
	 */
	private Player player;
	/**
	 * The speed at which the camera will follow the player. This value must be superior or equals to 0; 0 would mean it follows instantly the player,
	 * and the higher it is, the slower it moves.
	 */
	public int FOLLOW_SPEED;
	/**
	 * If the WorldView is currently paused. If it is true, the background will still render, but a gray overlay and button will appear on top.
	 */
	private boolean paused;
	/**
	 * The color that will fill the screen when the game is paused.
	 */
	private static final Color PAUSE_COLOR = new Color(50, 50, 50, 130);
	/**
	 * The button to allow the user to resume playing. This is only displayed when the screen in paused.
	 */
	private final Button resume;
	/**
	 * The button to allow the player to go bacl to the main menu. This is only displayed when the screen is paused.
	 */
	private final Button mainMenu;
	/**
	 * The total amount of frames that were drawn.
	 */
	private int frames = 1;
	/**
	 * The coordinate for the upper left corner of the first bar (both X/Y).
	 */
	private int barPos;
	/**
	 * The height of the bar.
	 */
	private int barHeight;
	/**
	 * The width of the bar.
	 */
	private int barWidth;
	/**
	 * The color to use to fill the back of the bars.
	 */
	private static final Color BAR_COLOR = new Color(0x275AC1);
	/**
	 * The color to use to fill the back of the bars when the Player has a size bonus.
	 */
	private static final Color BAR_COLOR_SIZE_PLUS = new Color(0xB4202A);
	/**
	 * A quick array with a bunch of values for cos.
	 */
	private final float[] cos;
	/**
	 * The pixel size for the bars.
	 */
	private float pixel;
	/**
	 * The bottom part of the bar, where the waves should end.
	 */
	private int yb;
	/**
	 * The height of the Height bar.
	 */
	private int hhb;
	/**
	 * The height of the Width bar.
	 */
	private int hwb;
	/**
	 * The minimum x coordinate to display the bars.
	 */
	private int barStart;
	/**
	 * The maximum x coordinate to display the bars.
	 */
	private int barEnd;
	 /**
	  * The font to display basic information, for instance the debug info.
	  */
	private Font smallFont;
	/**
	 * The font to display the current coin count/
	 */
	private Font coinFont;
	/**
	 * The formatted String that displays, with leading zeroes, the amount of coins the Player has.
	 */
	private String count;
	/**
	 * The Y coordinate to display the String and the icon, for the coin indicator.
	 */
	private int yCordCoin;
	/**
	 * The X coordinate to display the String and the icon, for the coin indicator.
	 */
	private int xCordCoin;
	/**
	 * The size of the coin icon, for the coin display.
	 */
	private int coinIconSize;
	/**
	 * The size in pixels the Player's head shold have.
	 */
	private int sizeFace;
	/**
	 * The width in pixels the Player has in total.
	 */
	private float wChar;
	/**
	 * The height in pixels the Player has in total.
	 */
	private float hChar;
	/**
	 * The width of one of Player's skin.
	 */
	private float sizeCharX;
	/**
	 * The height of one of Player's skin.
	 */
	private float sizeCharY;
	

	/**
	 * Created a WorldView panel, that will show the position of the player in the given world.
	 * @param w The world in which the player should be located. This panel won't check that the player is in this
	 * world, so using a world that isn't the player's should be avoided.
	 * @param window 
	 */
	public WorldView(World w, Window window) {
		this.FOLLOW_SPEED = 10;
		this.world = w;
		this.player = WhereItEnds.INSTANCE.PLAYER;
		this.smallFont = WhereItEnds.NORMAL_FONT.deriveFont(Font.PLAIN, 16);
		this.coinFont = WhereItEnds.NORMAL_FONT.deriveFont(Font.PLAIN, 40);
		this.zoom = 30;
		this.mother = window;
		this.count = "000";
		this.blocksX = this.zoom + 4;
		this.widthH = getWidth()/2;
		this.heightH = getHeight()/2;
		this.center = this.player.getLocation().get();
		this.resume = new Button("Resume", this, 0.5f, 0.4f);
		this.mainMenu = new Button("Main Menu", this, 0.5f, 0.7f);
		this.resume.addButtonListener(this);
		this.mainMenu.addButtonListener(this);

		this.cos = new float[360];
		for(int i = 0; i < 360; i++)
			this.cos[i] = (float) Math.cos(Math.toRadians(i));
		
		this.addMouseWheelListener(this);
		this.addComponentListener(this);
		
	}
	
	/**
	 * Will load the necessary Images for the GUI.
	 * @param cl The class loader for the images.
	 * @throws IOException
	 */
	public static void load(ClassLoader cl) throws IOException {
		BAR_BACK = ImageIO.read(cl.getResourceAsStream("assets/gui/bar_back.png"));
		BAR_FRONT = ImageIO.read(cl.getResourceAsStream("assets/gui/bar.png"));
	}
	
	/**
	 * @return If the WorldView is currently paused. If it is true, the background will still render, but a gray overlay and button will appear on top.
	 */
	public boolean isPaused() {
		return this.paused;
	}
	
	/**
	 * Will set the pause state of the screen.
	 * @param paused If it is paused.
	 */
	public void setPaused(boolean isPaused) {
		this.paused = isPaused;
		this.mainMenu.setListening(this.paused);
		this.resume.setListening(this.paused);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		final float x = this.player.getLocation().x;
		final float y = this.player.getLocation().y;
		
		g.setFont(this.smallFont);
		boolean showBounds = Keyboard.isPressed(KeyEvent.VK_Q);
		
		// Sky
		g.setColor(this.world.getColor());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// Character
		int xCharS = (int) (x(x) - this.wChar/2);
		int yCharS = (int) (y(y) - this.hChar/2);

		for(float xChar = xCharS; Math.ceil(xChar) < Math.floor(xCharS + this.wChar); xChar += this.sizeCharX)
			for(float yChar = yCharS; Math.ceil(yChar) < Math.floor(yCharS + this.hChar); yChar += this.sizeCharY)
				g.drawImage(this.player.getBodyTexture(), (int) Math.ceil(xChar), (int) Math.ceil(yChar), (int) Math.ceil(this.sizeCharX), (int) Math.ceil(this.sizeCharY), this);
				
		g.drawImage(this.player.getCurrentFace(), 
				(int) (xCharS + this.wChar/2 - this.sizeFace/2), 
				(int) (yCharS + this.hChar/2 - this.sizeFace/2), 
				this.sizeFace, this.sizeFace, this);
		
		// Blocks
		g.setColor(Color.BLACK);
		for (int bx = (int) this.center.x - this.blocksX / 2; bx < this.center.x + this.blocksX / 2; bx++) {
			int rendX = x(bx);
		for (int by = (int) this.center.y - this.blocksY / 2; by < this.center.y + this.blocksY / 2; by++) {
			int rendY = y(by);
				Block b = this.world.get(bx, by);
				g.drawImage(b.getImage(this.frames), rendX, rendY, this.sizeBlock, this.sizeBlock, this);
				if(showBounds && this.zoom <= 35) {
					g.drawString("x " + bx, rendX+3, rendY+10);
					g.drawString("y " + by, rendX+3, rendY+20);
				}
			}
		}
		
		for(Particle p : this.world.getParticles()) {
			g.setColor(p.getColor());
			int wP = (int) (p.getWidth()*this.sizeBlock);
			int hP = (int) (p.getWidth()*this.sizeBlock);
			int xP = x(p.getLocation().x);
			int yP = y(p.getLocation().y);
			if(p.getRotation() == 0)
				g.fillRect(xP-wP/2, yP-hP/2, wP, hP);
			else {
				int[][] coordinates = Util.rotateRectangle(xP, yP, wP, hP, p.getRotation());
				g.fillPolygon(coordinates[0], coordinates[1], 4);
			}
		}
		
		// Debug Bounds 
		if(showBounds) {
			for (int bx = (int) this.center.x - this.blocksX / 2; bx < this.center.x + this.blocksX / 2; bx++) {
				int rendX = x(bx);
				if(bx % Chunk.X_SIZE == 0) {
					g.setColor(Color.RED);
					g.fillRect(rendX-1, 0, 3, getHeight());
					g.setColor(Color.BLACK);
				} else
					g.drawLine(rendX, 0, rendX, getHeight());
			}
			for (int by = (int) this.center.y - this.blocksY / 2; by < this.center.y + this.blocksY / 2; by++) {
				int rendY = y(by);
				g.drawLine(0, rendY, getWidth(), rendY);
			}
		}
		
		// Bars
		g.setColor(this.player.getCurrentEffect() == Effect.SIZE_PLUS ? BAR_COLOR_SIZE_PLUS : BAR_COLOR);
		g.drawImage(BAR_BACK, this.barPos, this.barPos, this.barWidth, this.barHeight, this);
		g.drawImage(BAR_BACK, this.barPos * 2 + this.barWidth, this.barPos, this.barWidth, this.barHeight, this);
		
		float inc = 200f/(this.barEnd-this.barStart);
		float deg = this.frames*4f;
		for (int i = this.barStart; i < this.barEnd; i++, deg += inc) {
			int val = (int) (this.cos[(int) (deg % 360)] * this.barHeight / 50.0);
			g.fillRect(i, this.yb, 1, this.hhb - val);
			g.fillRect(i + this.barPos + this.barWidth, this.yb, 1, this.hwb - val);
		}
		
		g.drawImage(BAR_FRONT, this.barPos, this.barPos, this.barWidth, this.barHeight, this);
		g.drawImage(BAR_FRONT, this.barPos*2+this.barWidth, this.barPos, this.barWidth, this.barHeight, this);
		
		if(this.player.getEffectTimer() > 0) {
			g.setColor(Color.GRAY);
			g.fillRect(this.barPos, this.barPos*2+this.barHeight, this.barWidth*2+this.barPos, this.barPos*2);
			g.setColor(WorldView.BAR_COLOR_SIZE_PLUS);
			g.fillRect(this.barPos, this.barPos*2+this.barHeight, (int)((this.barWidth*2.0+this.barPos)*((double)this.player.getEffectTimer()/this.player.getMaxEffectTimer())), this.barPos*2);
		}

		// Debug Data
		if(Keyboard.isEnabled(KeyEvent.VK_F3)) {
			String[] data = {
					"World: " + this.world.getName(),
					"Seed: " + this.world.getSeed(), 
					"",
					"Position:",
					"             " + this.player.getLocation().x,
					"             " + this.player.getLocation().y,
					"Velocity: ",
					"             " + this.player.getVelocity().x,
					"             " + this.player.getVelocity().y,
					"Zoom: " + this.zoom, 
					"FPS: " + this.mother.FRAMES,
					"Particles: " + this.world.getParticles().size()
			};
			g.setColor(Color.BLACK);
			int yTxt = this.barPos+10;
			for(String line : data) {
				g.drawString(line, this.barPos*3+this.barWidth*2, yTxt);
				yTxt += 15;
			}		
		}
		
		// Coin Counter
		g.setFont(this.coinFont);
		g.setColor(Color.black);
		g.drawString(this.count, this.xCordCoin, this.yCordCoin);
		g.drawImage(Block.COIN.getImage(this.frames), this.xCordCoin - this.coinIconSize, this.yCordCoin-this.coinIconSize, this.coinIconSize, this.coinIconSize, this);
		
		if(this.paused) {
			g.setColor(PAUSE_COLOR);
			g.fillRect(0, 0, getWidth(), getHeight());
			this.mainMenu.paint(g);
			this.resume.paint(g);
		} else {
			this.frames++;
			this.center.x = (this.center.x * this.FOLLOW_SPEED + x) / (this.FOLLOW_SPEED+1);
			this.center.y = (this.center.y * this.FOLLOW_SPEED + y) / (this.FOLLOW_SPEED+1);
		}
		this.mother.CURRENT_FRAMES++;
	}
	
	
	
	/**
	 * @param where The X coordinate that is will be placed.
	 * @return The X pixel position where a given X coordinate is located, when considering that the first float
	 * is the X coordinate of the middle of the screen.
	 */
	private int x(float where) {
		return this.widthH + (int)Math.floor((where - this.center.x) * this.sizeBlock);
	}
	
	/**
	 * @param where The Y coordinate that is will be placed.
	 * @return The Y pixel position where a given Y coordinate is located, when considering that the first float
	 * is the Y coordinate of the middle of the screen.
	 */
	private int y(float where) {
		return this.heightH - (int) Math.floor((where - this.center.y) * this.sizeBlock);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(!this.paused) {
			this.zoom = (byte) (this.zoom + e.getWheelRotation());
			if (this.zoom < 1)
				this.zoom = 1;
			if (this.zoom > 120)
				this.zoom = 120;
			this.blocksX = this.zoom + 4;
			this.sizeBlock = (int) Math.ceil((double) getWidth() / (this.blocksX-4));
			this.blocksY = (int) Math.ceil((double) getHeight() / this.sizeBlock) + 4;
			changedCharacterWidth(this.player.getWidth());
			changedCharacterHeight(this.player.getHeight());
		}
	}
	
	@Override
	public void componentResized(ComponentEvent evt) {
		this.widthH = getWidth()/2;
		this.heightH = getHeight()/2;
		this.sizeBlock = (int) Math.ceil((double) getWidth() / (this.blocksX-4));
		this.blocksY = (int) Math.ceil((double) getHeight() / this.sizeBlock) + 4;

		this.barHeight = (int) (getHeight()/3.5);
		this.barHeight -= this.barHeight%3;
		this.barWidth = this.barHeight/3;
		this.barPos = this.barWidth/4;
		this.pixel = (float)this.barHeight/60;
		this.yb = (int)Math.ceil(this.barPos+this.barHeight-this.pixel*2);
		this.barStart = (int) (this.barPos + this.pixel * 2);
		this.barEnd = (int) Math.ceil(this.barPos + this.barWidth - this.pixel * 2);
		
		changedCharacterWidth(this.player.getWidth());
		changedCharacterHeight(this.player.getHeight());
		
		this.coinFont = this.coinFont.deriveFont(getHeight()/7f);
		this.count = String.format("%03d", this.player.getCoinsTaken());
		this.yCordCoin = getFontMetrics(this.coinFont).getAscent();
		this.xCordCoin = (int) (getWidth() - getFontMetrics(this.coinFont).stringWidth(this.count) * 1.1);
		this.coinIconSize = (int) (this.yCordCoin * 0.7);
		
		this.frames--;
	}
	
	/**
	 * Will recalculate the new coordinates etc. for the coin display.
	 * @param newCount The new coin count.
	 */
	public void changedCoinCount(int newCount) {
		this.count = String.format("%03d", newCount);
		this.xCordCoin = (int) (getWidth() - getFontMetrics(this.coinFont).stringWidth(this.count) * 1.1);
	}
	
	/**
	 * Will recalculate the necessary values for the display of the player's character. This needs to be called whenever
	 * its width is changed, or when the zoom and window size is changed. 
	 * @param w The new width.
	 */
	public void changedCharacterWidth(float w) {
		this.wChar = this.player.getWidth() * this.sizeBlock;
		this.sizeCharX = this.wChar / (float) Math.floor(this.player.getWidth() < 1 ? 1 : this.player.getWidth());
		this.sizeFace = this.sizeCharX < this.sizeBlock || this.sizeCharY < this.sizeBlock ? (int) Math.min(this.sizeCharX, this.sizeCharY) : this.sizeBlock;
		this.hwb = -(int)((this.barHeight-this.pixel*4)/this.player.getMaxSize()*w);
	}
	
	/**
	 * Will recalculate the necessary values for the display of the player's character. This needs to be called whenever
	 * its height is changed, or when the zoom and window size is changed.
	 * @param h The new height.
	 */
	public void changedCharacterHeight(float h) {
		this.hChar = this.player.getHeight() * this.sizeBlock;
		this.sizeCharY = this.hChar / (float) Math.floor(this.player.getHeight() < 1 ? 1 : this.player.getHeight());
		this.sizeFace = this.sizeCharX < this.sizeBlock || this.sizeCharY < this.sizeBlock ? (int) Math.min(this.sizeCharX, this.sizeCharY) : this.sizeBlock;
		this.hhb = -(int)((this.barHeight-this.pixel*4)/this.player.getMaxSize()*h);
	}
	
	@Override
	public void buttonPressed(UIComponent b) {
		if(b == this.resume) {
			setPaused(false);
		} else {
			try {
				this.world.saveTo(WhereItEnds.INSTANCE.SAVE_FOLDER);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mother.switchToWorldSelectMenu();
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		return this.mother.getSize();
	}

	@Override
	public void componentHidden(ComponentEvent e) {/*Not used*/}

	@Override
	public void componentMoved(ComponentEvent e) {/*Not used*/}

	@Override
	public void componentShown(ComponentEvent e) {/*Not used*/}
}
